<?php
require_once('../assets/includes/core.php');

$all_chats = userChats($user_id);

foreach ($all_chats as $row) {
	
	//GET INFO FROM THE USER THAT IS CHATTING
	$chat_user = $mysqli->query("SELECT * FROM usuarios where id = '".$row."'");
	$c_user = $chat_user->fetch_object();
	
	//GET LAST MESSAGE OF THE CONVERSATION
	$chatt = $mysqli->query("SELECT * FROM chat where s_id = '".$sm['user']['id']."' and r_id = '".$c_user->id."' 
							OR r_id = '".$sm['user']['id']."' and s_id = '".$c_user->id."' order by id desc limit 1");
	$chat_men = $chatt->fetch_object();
	
	//SHOW BUBBLE WITH LAST MESSAGE AND PHOTO
	echo '<div class="notification-page-item" id="chat'.$c_user->id.'"><img src="'.profilePhoto($c_user->id).'" alt="img">';
	
	//IF LAST MESSAGE IS TOO LONG, SHOW ONLY 66 CHARACTERS			
	$chat_menssage = substr($chat_men->mensaje, 0, 66);
	$long = strlen($chat_men->mensaje);
	if($long > 66){
		$chat_menssage = $chat_menssage.'...';
	}
	
	//CHECK IF MESSAGE HAS BEEN SEEN BEFORE OR ITS NEW
	if($chat_men->s_id != $sm['user']['id'] && $chat_men->visto == 0){
		 echo'<em>'.$chat_menssage.' <span style="color:#26a518;"> ('.$lang['chat_new_message'].')</span></em>';				
	} else{
		echo'<em><span id="ultimo_mensaje'.$row.'">'.$chat_menssage.'</span></em>';		
	}		
	echo'<a href="#'.$c_user->nombre.'" onClick="chat_user_ingame_mobile('.$sm['user']['id'].','.$c_user->id.','; echo "'".$c_user->nombre."'"; echo ')"> 
	<i class="fa fa-comment-o"></i> '.$lang['chat_now'].'</a> <div class="decoration hide-if-responsive"></div></div>';
}
