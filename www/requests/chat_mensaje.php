<?php
header('Content-Type: application/json');
require_once('../assets/includes/core.php');

$uid = $user_id;
$array1  = array();

//SET CHAT MENU NOTIFICATION TO 0
$mysqli->query("UPDATE usuarios SET chat = 0 where id = '".$uid."'");

//CHECK IF USER HAVE NEW MESSAGES
$results = $mysqli->query("SELECT * FROM chat where r_id = '".$uid."' and visto = 0  GROUP BY s_id ");
if($results->num_rows > 0){
	while ($obj = $results->fetch_object()){
		
		//GET SENDER INFO
		$s_u = $mysqli->query("SELECT * FROM usuarios where id = '".$obj->s_id."'");
		$us = $s_u->fetch_object();	
		
		//MESSAGE
		$chat_menssage = substr($obj->mensaje, 0, 66);
		$long = strlen($obj->mensaje);
		if($long > 66){
			$chat_menssage = $chat_menssage.'...';
		}
				
		$name = "'".$us->nombre."'";
		$array1[] = array(
		"id" => $obj->s_id,
		"message" => $chat_menssage,
		"full" => '
			<ul class="chats" id="chat'.$us->id.'" style="padding-top:50px;">
			<li class="you" style="width:100%;">
			<div class="image" >
			<span style="position:absolute; left:15px;">'.status($us->id,15).'</span>
			<a href="#'.$us->nombre.'" onClick="chat_user('.$uid.','.$us->id.','.$name.')">
			<img  src="'.profilePhoto($us->id).'"/></a>
			</div>
			<div class="bubble green" id="ultimo_mensaje'.$us->id.'"  style="position:absolute; left:90px; border:none;"  >
			<span style="font-size: 0.95em;line-height: 1.25em;#414141;">'.$chat_menssage.'</span>
			</div>
			</li>
			</ul> 
			',
		);						
	}	
	//UPDATE MESAGGE AS RECIVED
	$mysqli->query("UPDATE chat SET enviado = 1 where r_id = '".$uid."'");					
}
	
echo json_encode($array1);

//CLOSE DB CONNECTION
$mysqli->close();
