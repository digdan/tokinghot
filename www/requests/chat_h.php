<?php
require_once('../assets/includes/core.php');
require_once('../assets/time/jquery.time.php');
//Get post info
$u1 = $_POST['u1'];
$u2 = $_POST['u2'];

//Secure info
$u1 = secureEncode($u1);	
$u2 = secureEncode($u2);

//Get all chats
$chatt = $mysqli->query("SELECT * FROM chat where s_id = '".$u1."' and r_id = '".$u2."' OR r_id = '".$u1."' and s_id = '".$u2."' order by id asc");

//Update chat to seen
$mysqli->query("UPDATE chat set visto = 1 where s_id = '".$u2."' and r_id = '".$u1."'");
if ($chatt->num_rows > 0) { 
	while($chat= $chatt->fetch_object()){
		//Get profile photo
		$mensaje = $chat->mensaje;	
		//Check if current user is the sender
		if($chat->s_id == $u1){
			
			if($chat->visto == 1){
				echo'
				<li class="me">
					<div class="image">
						<a class="hint--left hint--success" data-hint="'.jquery_time($chat->fecha).', '.$lang['message_seen'].'" href="javascript:void(0)">
						<img src="'.profilePhoto($chat->s_id).'"/></a>
					</div>
					<p> '.$mensaje.' </p>	
				</li> 
				';	
			} else {
				echo'
				<li class="me">
					<div class="image">
						<a class="hint--left" data-hint="'.jquery_time($chat->fecha).', '.$lang['message_unseen'].'" href="javascript:void(0)">
						<img src="'.profilePhoto($chat->s_id).'"/></a>
					</div>
					<p> '.$mensaje.' </p>	
				</li> 
				';							
			}
			
		} else {
			echo'									
			<li class="you">
			<div class="image">
			<a class="hint--bottom" data-hint="'.jquery_time($chat->fecha).'" href="javascript:void(0)">
			<img src="'.profilePhoto($chat->s_id).'"/></a>
			</div>
			<p> '.$mensaje.' </p>	
			</li> 
			';					
		}						
		//Check if is first message of conversation for prevent spam
		$total_m = $mysqli->query("SELECT id FROM chat WHERE s_id = '".$u1."' AND r_id = '".$u2."'");
	
		if($total_m->num_rows == 1 && $chatt->num_rows == 1){
			echo '<li class="info" id="prevent_spam">
					<p>'.$lang['chat_first_message_info'].'</p>
				</li>';
		}					
	}		
}

//CLOSE DB CONNECTION
$mysqli->close();				

