<?php
header('Content-Type: application/json');
require_once('../assets/includes/core.php');

$uid = $user_id;
$array1  = array();

//CHECK IF USER HAVE NEW MESSAGES
$results = $mysqli->query("SELECT * FROM chat where r_id = '".$uid."' and visto = 0  GROUP BY s_id ");

if($results->num_rows > 0){
	while ($obj = $results->fetch_object()){
		
		//GET SENDER INFO
		$s_u = $mysqli->query("SELECT * FROM usuarios where id = '".$obj->s_id."'");
		$us = $s_u->fetch_object();	
		
		//MESSAGE
		$chat_menssage = substr($obj->mensaje, 0, 66);
		$long = strlen($obj->mensaje);
		
		if($long > 66){
			$chat_menssage = $chat_menssage.'...';
		}	
		
		$name = "'".$us->nombre."'";
		$array1[] = array(
		"id" => $obj->s_id,
		"message" => $chat_menssage,
		"full" => '
			<div class="notification-page-item" id="chat'.$us->id.'">
			<img src="'.profilePhoto($us->id).'" alt="img">
			<em >
				<span id="ultimo_mensaje'.$us->id.'">'.$chat_menssage.'</span> <span style="color:#26a518;"> (new)</span>
			</em>
			<a href="#'.$us->nombre.'"  onClick="chat_user_ingame_mobile('.$uid.','.$us->id.','.$name.')">
			<i class="fa fa-comment-o"></i> Chat now</a>
			<div class="decoration hide-if-responsive"></div></div>				
			',
		);					
	}
	//UPDATE MESAGGE AS RECIVED
	$mysqli->query("UPDATE chat SET enviado = 1 where r_id = '".$uid."'");					
}
	
echo json_encode($array1);

//CLOSE DB CONNECTION
$mysqli->close();
