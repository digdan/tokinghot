<?php
require_once('../assets/includes/core.php');
require_once('../assets/time/jquery.time.php');

//GET POST INFO
$u1 = secureEncode($_POST['u1']);	
$u2 = secureEncode($_POST['u2']);

//GET NEW MESSAGES
$results = $mysqli->query("SELECT s_id,fecha,mensaje FROM chat WHERE r_id = '".$u1."' AND s_id = '".$u2."' AND visto = 0  order by id asc");


//CHECK IF CURRENT USER HAS NEW MESSAGE
if($results->num_rows > 0){		
	while ($obj = $results->fetch_object()){
		
		//UPDATE MESSAGE AS SEEN
		$mysqli->query("UPDATE chat SET visto = 1 where r_id = '".$u1."' and s_id = '".$u2."'");
		
		//SHOW NEW MESSAGES
		echo'
		<li class="you">
            <div class="image">
                <a class="hint--bottom" data-hint="'.jquery_time($obj->fecha).'" href="javascript:void(0)">
                <img src="'.profilePhoto($obj->s_id).'"/></a>
            </div>
            <p> '.$obj->mensaje.' </p>
		</li> 
		';		
	}
}

$mysqli->close();
