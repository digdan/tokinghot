<?php 
require_once('../assets/includes/core.php');

if($_POST){
	
	//GET POST INFO
	$login = secureEncode($_POST['email']);
	$password = secureEncode($_POST['pass']);
	
	//CHECK IF EMAIL EXIST
	$email_check = $mysqli->query("SELECT * FROM usuarios WHERE email = '".$login."'");

	if($email_check->num_rows == 0 ){
		echo 'Error - '.$lang['login_user_not_exist'];	
		exit;
	}
	//GET PASS OF USER AND CHECK IF OK
	$pass = $email_check->fetch_object();
	if(crypt($password, $pass->pass) == $pass->pass) { 
		$_SESSION['user'] = $pass->id;
		if($pass->admin == 1){
			echo 'admin'; 
		}
		exit;	
	} else {
		echo 'Error - '.$lang['login_wrong_password'];	
		exit;		
	}
}

//CLOSE DB CONNECTION
$mysqli->close();

