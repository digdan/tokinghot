<?php
// Sexymetro - A Hot or Not Platform

require_once('assets/includes/core.php');

$mobile = false;
if (!isset($_GET['page'])) {
    $_GET['page'] = 'preview';
}


if($logged == true && empty($sm['user']['age'])) {
	include('assets/sources/preferences.php');	
} else {
	switch ($_GET['page']) {
		
		// Preview page source
		case 'preview':
			include('assets/sources/preview.php');
			exit;
		break;
		
		// Login page source
		case 'login':
			include('assets/sources/login.php');
			exit;
		break;	
		
		// Forgot page source
		case 'forgot':
			include('assets/sources/forgot.php');
			exit;
		break;	
		
		// Change password page source
		case 'changePassword':
			include('assets/sources/reset_password.php');
			exit;
		break;			
		
		// Facebook Connect page source
		case 'fbconnect':
			include('assets/sources/fbconnect.php');
			exit;
		break;		
		// Create account page source
		case 'account':
			include('assets/sources/account.php');
			exit;
		break;		
		
		// Sexy game page source
		case 'game':
			include('assets/sources/index.php');
		break;
		
		// Meet page source
		case 'meet':
			include('assets/sources/meet.php');
		break;
		
		// Chat page source
		case 'chat':
			include('assets/sources/chat.php');
		break;
		
		// Crushes page source
		case 'crushes':
			include('assets/sources/crushes.php');
		break;	
		
		// Profile page source
		case 'profile':
			include('assets/sources/profile.php');
		break;
		
		// Preferences page source
		case 'settings':
			include('assets/sources/preferences.php');
		break;
	
		// Album page source
		case 'add_photos':
			include('assets/sources/add_photos.php');
		break;
		
		// Socials page source
		case 'socials':
			include('assets/sources/socials.php');
		break;
		
		// Chnage pass page source
		case 'change_password':
			include('assets/sources/change_password.php');
		break;
		
		// Admin page source
		case 'admin':
			include('assets/sources/admin_index.php');
		break;
		
		// Admin view users page source
		case 'admin_users':
			include('assets/sources/admin_users.php');
		break;
		
		// Admin manage users page source	
		case 'manage_user':
			include('assets/sources/admin_manage_user.php');
		break;	
			
		// Admin view/manage photos page source
		case 'admin_photos':
			include('assets/sources/admin_photos.php');
		break;		
		
		// Logout page source
		case 'logout':
			include('assets/sources/logout.php');
		break;
		
		
	}
}
// If no sources found
if (empty($sm['content'])) {
    echo getPage('preview/content');
	exit;
}

echo getPage('container');
$mysqli->close();
