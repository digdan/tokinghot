<?php
// Sexymetro - A Hot or Not Platform

require_once('assets/includes/core.php');

$mobile = true;
if (!isset($_GET['page'])) {
    $_GET['page'] = 'preview';
}

if($logged == true && empty($sm['user']['age'])) {
	include('assets/sources/preferences.php');
	$_GET['page'] = "settings";
} else {
	switch ($_GET['page']) {
		
		// Preview page source
		case 'preview':
			include('assets/sources/preview.php');
			exit;
		break;
		
		// Preview page source
		case 'login':
			include('assets/sources/login.php');
			exit;
		break;	
		// Facebook connect
		case 'fbconnect':
			include('assets/sources/fbconnect.php');
			exit;
		break;		
		// Account page source
		case 'account':
			include('assets/sources/account.php');
			exit;
		break;		
		
		// Sexy game page source
		case 'game':
			include('assets/sources/index.php');
		break;
		
		// Meet page source
		case 'meet':
			include('assets/sources/meet.php');
		break;
		
		// Chat page source
		case 'chat':
			include('assets/sources/chat.php');
		break;
		
		// Chat page source
		case 'crushes':
			include('assets/sources/crushes.php');
		break;	
		
		// Profile page source
		case 'profile':
			include('assets/sources/profile.php');
		break;
		
		// Preferences page source
		case 'settings':
			include('assets/sources/preferences.php');
		break;
	
		// Album page source
		case 'add_photos':
			include('assets/sources/add_photos.php');
		break;
		
		// Socials page source
		case 'socials':
			include('assets/sources/socials.php');
		break;
		
		// Chnage pass page source
		case 'change_password':
			include('assets/sources/change_password.php');
		break;
				
		
		// Logout page source
		case 'logout':
			include('assets/sources/logout.php');
		break;
		
		
	}
}
// If no sources found
if (empty($sm['content'])) {
    echo getMobilePage('preview/content');
	exit;
}

echo getMobilePage('container');
$mysqli->close();