// JavaScript Document

(function($){

$(document).ready(function(){

	window.addEventListener('load', function() {
		FastClick.attach(document.body);
	}, false);	
	

	$('.deploy-nav-sub-item').click(function(extended_menu){	
		$(this).parent().find('.nav-sub-item').slideToggle(200);
		var navItem = $(this).parent().offset().top -  $(this).parent().parent().offset().top;
		$('.outer-nav').animate({
			scrollTop: navItem
		}, 200);	
		console.log(navItem);
		return false;
	});
	/////////////////////////////////////////////////////////////////////////////////////////////
	//Detect user agent for known mobile devices and show hide elements for each specific element
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	var isiPhone = 		navigator.userAgent.toLowerCase().indexOf("iphone");
	var isiPad = 		navigator.userAgent.toLowerCase().indexOf("ipad");
	var isiPod = 		navigator.userAgent.toLowerCase().indexOf("ipod");
	var isiAndroid = 	navigator.userAgent.toLowerCase().indexOf("android");
	
	if(isiPhone > -1 || isiPad > -1 || isiPod > -1 || isiAndroid > -1)
		{
		$('.ipod-detected').hide();		 $('.ipad-detected').hide();		 $('.iphone-detected').show();		 $('.android-detected').hide();	
		}


});

}(jQuery));