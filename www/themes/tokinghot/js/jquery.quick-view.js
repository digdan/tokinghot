/**
 * jQuery - QuickView plugin file.
 */
(function($)
{
    //OddsMenu class
    function QuickView($holder, settings)
    {
        this.holder = $holder;
        this.previewImage = this.holder.find('*[data-quick-view="previewImage"]');
        this.thumbs = this.holder.find('*[data-image]');
        this.quickNav = this.holder.find('*[data-quick-view="quickNav"]');
        this.settings = $.extend({}, $.fn.quickView.defaults, settings || {});
        this.actualIndex = 0;
        this.timer = null;
        this.hiddenThumbs = this.holder.hasClass('hiddenThumbs') && this.quickNav.length == 0;
    }

    //QuickView class methods
    $.extend(QuickView.prototype,
    {
        /**
         * init QuickView
         */
        init:function()
        {
            var isTouchDevice = ('ontouchstart' in document.documentElement);
            //hidden thumbs not possible on touch devices
            if (isTouchDevice && this.hiddenThumbs)
            {
                this.holder.removeClass('hiddenThumbs');
            }

            //thumbs
            this.thumbs.each($.proxy(function(index, element) {
                var $element = $(element);
                $element.mouseenter($.proxy(function() {
                    this.show(index);
                }, this));
                $element.mouseleave($.proxy(function() {
                    this.clear();
                }, this));

                $element.find('a').click(function(event) { event.preventDefault(); return false; });

                //preload image
                if($element.find('img').length == 0 && !$element.data('preload-image'))
                {
                    var image = new Image();
                    image.src = $element.data('image');
                }
            }, this));

            //activate first thumb
            if(this.thumbs.find('.active').length == 0)
            {
                $(this.thumbs[0]).addClass('active');
            }

//            this.holder.css('float', 'left');
//            var width = this.holder.outerWidth();
//            this.holder.attr('style', '');
//            this.holder.width(width);

            //quick nav
            if(this.quickNav.length > 0)
            {
                this.quickNav.css('left', (this.quickNav.parent().outerWidth() - this.quickNav.outerWidth()) / 2);
            }



            //scale
            var scaleObj = this.holder.scale();

            //slider
            this.slider = $('<span/>');
            this.previewImage.parent().append(this.slider);
            this.slider.append(this.previewImage);
            this.slider.width(this.previewImage.width());
            this.slider.height(this.previewImage.height());
            this.slider.css('overflow', 'hidden');
            this.slider.css('display', 'block');
            this.thumbs.each($.proxy(function(index, element) {
                var $element = $(element);
                if(!$element.hasClass('active'))
                {
                    this.slider.append($('<img src="' + $element.data('image') + '"/>'));
                }
            }, this));

            var settings = {};
            if(!this.holder.hasClass('noThumbs'))
            {
                settings['createNavigation'] = false;
            }
            if(isTouchDevice)
            {
                settings['wipeLeftCallback'] = $.proxy(this.wipeMove, this);
                settings['wipeRightCallback'] = $.proxy(this.wipeMove, this);
            }
            this.slider.horizontalSlider(settings);
            var sliderObj = this.slider.horizontalSlider('get');
            sliderObj.scaleObj = scaleObj;

            //autoplay
            if(this.settings.autoPlay.active)
            {
                this.slider.mouseenter($.proxy(this.play, this));
                this.slider.mouseleave($.proxy(function() { this.stop(true); }, this));
                this.slider.parent().mouseleave($.proxy(function() { this.stop(); }, this));
            }

        },

        wipeMove: function()
        {
            var items = this.quickNav.find('li');
            items.removeClass('active');
            $(items[this.slider.horizontalSlider('get').selectedIndex]).addClass('active');

            this.holder.addClass('play');
            this.thumbs.removeClass('active');
            $(this.thumbs[this.slider.horizontalSlider('get').selectedIndex]).addClass('active');

        },

        //show image
        show: function(index)
        {
            this.actualIndex = index;

            var element = $(this.thumbs[index]);
            this.thumbs.removeClass('active');
            element.addClass('active');

            this.slider.horizontalSlider('get').selectItem(this.actualIndex, true);

        },

        //clear image
        clear: function(skipReturnToFirst)
        {
            if(this.settings.returnToFirst && !skipReturnToFirst)
            {
                this.show(0);
            }
        },

        //play
        play: function()
        {
            this.stop(true);
            this.timer = setInterval($.proxy(function() {
                this.show((this.actualIndex + 1) % this.thumbs.length);
            }, this), this.settings.autoPlay.delay);
            this.holder.addClass('play');
        },

        //stop
        stop: function(skipReturnToFirst)
        {
            if(this.timer) clearInterval(this.timer);
            this.clear(skipReturnToFirst);
            this.holder.removeClass('play');
        }


    });

    //plugin code
    $.extend($.fn,
    {
        quickView:function(options)
        {
            var ret;
            this.each(function()
            {
                var $this = $(this);

                var quickView =  $this.data('quickView');
                if (!quickView)
                {
					quickView = new QuickView($this, options);
                    quickView.init();
					$this.data('quickView', quickView);
				}
                ret = ret ? ret.add($this) : $this;
            });
            return ret;
        }

    });

     $.fn.quickView.defaults = {
         returnToFirst: true,
         autoPlay: {
            active: true,
            delay: 2000
         }
    };
})(jQuery);

/**
 * jQuery - Scale plugin file.
 */
(function($)
{
    //OddsMenu class
    function Scale($element, settings)
    {
        this.holder = null;
        this.scaleHolder = null;
        this.element = $element;
        this.settings = $.extend({}, $.fn.scale.defaults, settings || {});
        this.factor = 1;
    }

    //Scale class methods
    $.extend(Scale.prototype,
    {
        /**
         * init Scale
         */
        init:function()
        {
            $(window).resize($.proxy(this.scale, this));
            this.scale();
        },

        //scale
        scale: function()
        {
            this.originalWidth = this.element.outerWidth(true);
            this.originalHeight = this.element.outerHeight(true);

            if($('body').width() > this.settings.documentWidthLimit)
            {
                this.clearHtml();
            }
            else
            {
                this.createHtml();

                this.factor = $('body').width() / this.originalWidth;
                this.scaleHolder.css({
                    WebkitTransform : 'scale(' + this.factor + ')',
                    MozTransform : 'scale(' + this.factor + ')',
                    MsTransform : 'scale(' + this.factor + ')',
                    OTransform : 'scale(' + this.factor + ')',
                    transform : 'scale(' + this.factor + ')',
                    WebkitTransformOrigin : '0 0',
                    MozTransformOrigin : '0 0',
                    MsTransformOrigin : '0 0',
                    OTransformOrigin : '0 0',
                    transformOrigin : '0 0',
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    height: this.originalHeight,
                    width: this.originalWidth

                });

                this.holder.css({
                    position: 'relative',
                    height: this.originalHeight * this.factor,
                    width: this.originalWidth * this.factor
                });
            }
        },

        createHtml: function()
        {
            if(this.holder && this.scaleHolder) return;

            this.holder = $('<div/>');
            this.holder.insertAfter(this.element);

            this.scaleHolder = $('<div/>');
            this.holder.html(this.scaleHolder);

            this.scaleHolder.html(this.element);
        },

        clearHtml: function()
        {
            if(!this.holder && !this.scaleHolder) return;

            this.element.insertAfter(this.holder);
            this.scaleHolder.remove();
            this.holder.remove();
            this.holder = null;
            this.scaleHolder = null;
        }


    });

    //plugin code
    $.extend($.fn,
    {
        scale:function(options)
        {
            var ret;
            this.each(function()
            {
                var $this = $(this);

                var scale =  $this.data('scale');
                if (!scale)
                {
					scale = new Scale($this, options);
                    scale.init();
					$this.data('scale', scale);
				}
                ret = ret ? ret.add($this) : $this;
            });
            return ret;
        }

    });

     $.fn.scale.defaults = {
         documentWidthLimit: 530

    };
})(jQuery);


//Slider_Abstract class
function Slider_Abstract()
{
    this.initialized = false;
}

(function($) {

//Slider_Abstract class methods
$.extend(Slider_Abstract.prototype,
{
    /**
     * set settings
     */
    setSettings: function(settings)
    {
        this.settings = $.extend({}, $.fn.slidable.defaults, settings || {});
    },

    /**
     * init
     */
    init: function($slider, settings)
    {
        this.setSettings(settings);
        if(!this.initialized)
        {
            this.pane = null;
            this.prevPageNav = null;
            this.nextPageNav = null;
            this.items = null;
            this.itemDimension = 0;
            this.animation = false;
            this.mouseWheelContainer = null;
            this.wipeTouch = null;
            this.slider = $slider;

            this.createHtml();
            this.initialized = true;
        }
        this.items = this.pane.children();

        this.cssAnimationSupport = this.supportsCssAnimation();
        this.bindEvents();
        this.initPanePosition();
        this.refreshItems(true);

        if(this.settings.autoSelect)
        {
            this.selectedIndex = 0;
            this.selectItem(0);
        }

    },


    /**
     * init pane position
     */
    initPanePosition: function()
    {
        this.setPanePosition(0);
    },

    resize: function()
    {
        this.refreshItems(true);
    },

    /**
     * refresh itesm
     */
    refreshItems: function(centerItem)
    {
        this.items = this.pane.children();

        if(this.settings.fullSizeItem)
        {
            for(var i = 0; i < this.items.length; i++)
            {
                $(this.items[i]).width(this.slider.width());
                $(this.items[i]).height(this.slider.height());
            }
        }

        if(this.items.length > 1)
        {
            this.itemDimension = this.getItemDimension();

            this.initPaneDimensions();
            this.initSliderDimensions();
            if(centerItem)
            {
                this.centerSelectedItem(false);
            }
            this.adjustNavigation();
        }
    },

    /**
     * addItem
     */
    addItem: function(item, position, centerItem)
    {
        if(this.cssAnimationSupport)
        {
            //webkit
            item.css("-webkit-backface-visibility","hidden");
            item.css("-webkit-perspective","1000");
        }


        if(position == 'before')
        {
            this.pane.prepend(item);
        }
        else
        {
            this.pane.append(item);
        }
        this.refreshItems(centerItem);

    },

    /**
     * removeItem
     */
    deleteItem: function(index, centerItem)
    {
        var item = $(this.items[index]);
        if(item)
        {
            item.remove();
            delete this.items[index];
            this.refreshItems(centerItem);
        }
    },

    /**
     * bindEvents
     */
    bindEvents: function()
    {
        this.resizeEvent = $.proxy(this.resize, this)
        $(window).resize(this.resizeEvent);

        //mouse wheel
        if(this.mouseWheelContainer)
        {
            this.mouseWheelContainer.unmousewheel();
        }
        if(this.settings.mouseWheelContainer)
        {
            this.mouseWheelContainer = this.settings.mouseWheelContainer;
            this.mouseWheelContainer.mousewheel($.proxy(this.mouseWheel, this));
        }

        if(this.settings.enableKeys)
        {
            this.keyDownHandler = $.proxy(function(event){ this.keyDown(event) }, this);
            $(document).keydown(this.keyDownHandler);
        }

        //wipe touch
        if(this.settings.wipeTouchActive)
        {
            var wipeTouchContainer = this.slider;
            wipeTouchContainer.wipetouch({
//                tapToClick: true,
//                moveX: this.settings.wipeTouchMinMove,
//                moveY: this.settings.wipeTouchMinMove,
//                allowVertical: this.allowVerticalWipeTouch,
//                allowHorizontal: this.allowHorizontalWipeTouch,
//                preventDefault: this.settings.wipePreventDefault,
//                wipeMove:  $.proxy(function(result) { this.wipeMove(result); }, this),
                wipeLeft:  $.proxy(function(result) { this.wipeNextPage(result); }, this),
                wipeRight: $.proxy(function(result) { this.wipePrevPage(result); }, this),
                wipeUp:  $.proxy(function(result) { this.wipePrevPage(result); }, this),
                wipeDown:  $.proxy(function(result) { this.wipeNextPage(result); }, this)
            });
            this.wipeTouch = wipeTouchContainer.wipetouch('get');
        }
    },

    unBindEvents: function()
    {
        if(this.keyDownHandler)
        {
            $(document).unbind('keydown', this.keyDownHandler);
        }

        if(this.resizeEvent)
        {
            $(window).unbind('resize', this.resizeEvent);
        }
    },

    /**
     * mouse wheel
     */
    mouseWheel: function(event, delta)
    {
        event.preventDefault();

        if(this.animation) return;

        if(delta < 0) this.nextPage();
        if(delta > 0) this.prevPage();
    },

    /**
     * key press
     */
    keyDown: function(event)
    {
        var code = event.charCode || event.keyCode;
        switch(code)
        {
            case 37: if(this.allowHorizontalKeys) { event.preventDefault(); this.prevPage() }; break;    //left arrow
            case 39: if(this.allowHorizontalKeys) { event.preventDefault(); this.nextPage() };  break;    //right arrow
            case 38: if(this.allowVerticalKeys) { event.preventDefault(); this.prevPage() };  break; //up arrow
            case 40: if(this.allowVerticalKeys) { event.preventDefault(); this.nextPage() };  break;//down arrow
        }
    },

    /**
     * wipe prev page
     */
    wipePrevPage: function(result)
    {
        if(this.animation) return;

        var itemsToMove = 1;
        var panePosition = this.getPanePosition();
        var paneOffset = this.getPaneOffset1();
        var move = itemsToMove * this.itemDimension;

        var settings = {
            effect: this.settings.wipeTouchEasingEffect,
            speed: this.settings.wipeTouchEasingSpeed
        };


        //ak je kladna pozicia - sme v elastickej oblasti, slider sa musi vratit na nulu
        if(panePosition >= 0)
        {
            move = 0;
        }
//        else
//        {
//            //offset vramci itemu - ked je slider posunuty nie o cely item;
//            var paneItemOffset = Math.ceil(panePosition / this.itemDimension) * this.itemDimension - panePosition;
//
//            //zarovna sa pane na cely dlzku itemu
//            move += paneItemOffset >= (this.itemDimension * this.settings.wipeTouchItemTreshold) ? paneItemOffset : (paneItemOffset - this.itemDimension);
//            move = Math.min(move, paneOffset);
//        }

        if(this.settings.wipeRightCallback)
        {
            settings.callback = this.settings.wipeRightCallback;
        }



        if(move != 0)
        {
            this.selectedIndex =  Math.max(0, this.selectedIndex - this.settings.itemsToMove);
            this.animate(panePosition + move, settings);
        }
    },

    /**
     * wipe next page
     */
    wipeNextPage: function(result)
    {
        if(this.animation) return;
        var itemsToMove = 1;
        var panePosition = this.getPanePosition();
        var paneOffset = this.getPaneOffset2();
        var move = itemsToMove * this.itemDimension;
        var maxMove = this.getSliderDimension() - this.getPaneDimension();

        var settings = {
            effect: this.settings.wipeTouchEasingEffect,
            speed: this.settings.wipeTouchEasingSpeed
        };

        //ak sme v elastickej oblasti, slider sa musi vratit spat
        if(panePosition <= maxMove)
        {
            move = 0;
        }
//        else
//        {
//            //offset vramci itemu - ked je slider posunuty nie o cely item;
//            var paneItemOffset = Math.ceil(panePosition / this.itemDimension) * this.itemDimension - panePosition;
//
//            //zarovna sa pane na celu dlzku itemu
//            move += paneItemOffset <= (this.itemDimension  * this.settings.wipeTouchItemTreshold) ? -paneItemOffset : (this.itemDimension - paneItemOffset);
//            move = Math.min(move, paneOffset);
//        }

        if(this.settings.wipeLeftCallback)
        {
            this.selectedIndex = Math.min(this.settings.itemsToMove + this.selectedIndex, this.items.length - 1);
            settings.callback = this.settings.wipeLeftCallback;
        }



        if(move != 0)
        {
            this.animate(panePosition - move, settings);
        }
    },

    rotateRight: function()
    {
        var items = this.pane.children();
        items[0].append(items[items.length - 1]);
        this.refreshItems(true);

    },

    rotateLeft: function()
    {
        var items = this.pane.children();
        items[items[items.length - 1]].prepend(items[0]);
        this.refreshItems(true);
    },


    /**
     * wipe move
     */
    wipeMove: function(result)
    {
        if(this.settings.disableWipeTouchAnimation)
        {
            return;
        }
        if(!this.settings.wipeTouchEnableMoveWhileAnimation && this.animation) return;

        if(this.animation)
        {
            this.stopAnimation();
        }

        var move = result.deltaX;
        var position = this.getPanePosition();
        var maxMove = this.getSliderDimension() - this.getPaneDimension();
        if(position + move > this.settings.wipeTouchElasticLength)
        {
            move = -posittion + this.settings.wipeTouchElasticLength;
        }
        if(position + move < maxMove - this.settings.wipeTouchElasticLength)
        {
            move = maxMove - position - this.settings.wipeTouchElasticLength;
        }
        this.setPanePosition(position + move);
    },

    /**
     * adjust navigation
     */
    adjustNavigation: function()
    {
        if(this.settings.createNavigation)
        {
            this.prevPageNav.addClass('hidden');
            this.nextPageNav.addClass('hidden');
            if( this.getSliderDimension() < this.getPaneDimension())
            {
                var paneOffset1 = this.getPaneOffset1();
                var paneOffset2 = this.getPaneOffset2();

                this.prevPageNav.removeClass('hidden');
                this.nextPageNav.removeClass('hidden');
                this.prevPageNav.removeClass('disabled');
                this.nextPageNav.removeClass('disabled');
                if(paneOffset1 == 0)
                {
                    this.prevPageNav.addClass('disabled');
                }
                if(paneOffset2 == 0)
                {
                    this.nextPageNav.addClass('disabled');
                }
            }
        }
    },

    /**
     * find selected
     */
    centerSelectedItem: function(animate)
    {
        var selectedElement = this.pane.find('.selected');
        if(selectedElement.length > 0)
        {
            var elementMargin = this.getElementMargin(selectedElement);
            var elementPosition = this.getElementPosition(selectedElement);
            var prevItemCount = (elementPosition + elementMargin) / this.itemDimension;
            var sliderDimension = Math.floor((this.getSliderDimension() + elementMargin) / this.itemDimension) * (this.itemDimension - 1);
            var prevItemInViewCount = Math.floor((sliderDimension / 2) / this.itemDimension);
            var itemsToMove = Math.max(prevItemCount - prevItemInViewCount, 0);

            var position = Math.min(itemsToMove * this.itemDimension, this.getPaneDimension() - this.getSliderDimension());
            if(animate)
            {
                if(parseInt(-position) != parseInt(this.getPanePosition()))
                {
                    this.fade(selectedElement, -position);
                }
            }
            else
            {
                this.setPanePosition(-position);
            }
        }
        return null;
    },


    /**
     * select item
     */
    selectItem: function(index, centerItem, animate)
    {
        if(index >= 0 && index < this.items.length)
        {
            this.selectedIndex = index;
            this.items.removeClass('selected');
            $(this.items[index]).addClass('selected');
            if(centerItem)
            {
                this.centerSelectedItem(animate);
            }
            this.adjustNavigation();
        }
    },

    /**
     * get pane left offset
     */
    getPaneOffset1: function()
    {
        return Math.abs(this.getPanePosition());
    },

    /**
     * get pane right offset
     */
    getPaneOffset2: function()
    {
        return this.getPaneDimension() + this.getPanePosition() - this.getSliderDimension();
    },

    /**
     * create sliding pane
     */
    createHtml: function()
    {
        var items = this.slider.children();

        this.slider.css('position', 'relative');
        this.slider.css('overflow', 'hidden');
        this.slider.addClass(this.sliderTypeClass);

        //pane
        this.pane = $('<span class="pane"/>');
        this.slider.append(
            this.pane
        );

        this.pane.css('display', 'block');
        this.pane.css('position', 'absolute');
        this.pane.css('left', 0);
        this.pane.css('top', 0);

        this.initPanePosition();

        items.each($.proxy(
            function(index, element)
            {
                this.pane.append($(element));
            }, this
        ));

        //navigation
        if(this.settings.createNavigation)
        {
            this.prevPageNav = $('<span class="sliderNavBt prevPage"/>').append($('<span class="icon"/>'));
            this.nextPageNav = $('<span class="sliderNavBt nextPage"/>').append($('<span class="icon"/>'));
            this.prevPageNav.insertBefore(this.slider);
            this.nextPageNav.insertAfter(this.slider);
            this.prevPageNav.addClass(this.sliderTypeClass);
            this.nextPageNav.addClass(this.sliderTypeClass);

            //events
            var isTouchDevice = ('ontouchstart' in document.documentElement);
            this.prevPageNav.click($.proxy(isTouchDevice ? this.prevPage : this.prevPageJump, this));
            this.nextPageNav.click($.proxy(isTouchDevice ? this.nextPage : this.nextPageJump, this));
        }
    },


    /**
     * previous page
     */
    prevPage: function(event)
    {
        if(event) {
            event.stopPropagation();
            event.preventDefault();
        }
        if(this.animation) return;
        if(this.selectedIndex == 0) return;


        var panePosition = this.getPanePosition();
        var paneOffset = this.getPaneOffset1();
        var move = this.settings.itemsToMove * this.itemDimension;

        move = Math.min(move, paneOffset);
        this.animate(panePosition + move);

        if(this.settings.autoSelect)
        {
            this.selectedIndex =  Math.max(0, this.selectedIndex - this.settings.itemsToMove);
//            this.selectItem(this.selectedIndex);
        }
    },

    prevPageJump: function(event)
    {
        if(event) {
            event.stopPropagation();
            event.preventDefault();
        }

        this.selectedIndex =  Math.max(0, this.selectedIndex - this.settings.itemsToMove);
        this.selectItem(this.selectedIndex, true);
    },

    /**
     * next page
     */
    nextPage: function(event)
    {
        if(event) {
            event.stopPropagation();
            event.preventDefault();
        }
        if(this.animation) return;
        if(this.selectedIndex == this.items.length - 1) return;

        var panePosition = this.getPanePosition();
        var paneOffset = this.getPaneOffset2();
        var move = this.settings.itemsToMove * this.itemDimension;
        move = Math.min(move, paneOffset);
        this.animate(panePosition - move);

        if(this.settings.autoSelect)
        {
            this.selectedIndex = Math.min(this.settings.itemsToMove + this.selectedIndex, this.items.length - 1);
//            this.selectItem(this.selectedIndex);
        }
    },

    nextPageJump: function(event)
    {
        if(event) {
            event.stopPropagation();
            event.preventDefault();
        }

        this.selectedIndex = Math.min(this.settings.itemsToMove + this.selectedIndex, this.items.length - 1);
        this.selectItem(this.selectedIndex, true);
    },

    /**
     * animation end
     */
    animationEnd: function(callback)
    {
        if(callback) callback.call();
        this.stopAnimation();
        this.adjustNavigation();
    },

    /**
     * stop animation
     */
    stopAnimation: function()
    {
        if(this.wipeTouch) this.wipeTouch.start();
        this.animation = false;
        this.pane.stop();

        if(this.cssAnimationSupport)
        {
            this.pane.unbind('webkitTransitionEnd'); //webkit
            this.pane.unbind('transitionend'); //firefox
            this.pane.unbind('oTransitionEnd'); //opera
            this.pane.unbind('MSTransitionEnd'); //IE 10
        }
    },

    /**
     * start animation
     */
    startAnimation: function()
    {
        if(this.wipeTouch) this.wipeTouch.stop();
        this.animation = true;
    },

    /**
     * supports css animation
     */
    supportsCssAnimation: function()
    {
        if(!this.settings.cssAnimation) return false;

        var b = document.body || document.documentElement;
        var s = b.style;
        var p = 'transition';
        if(typeof s[p] == 'string') {return true; }

        // Tests for vendor specific prop
        v = ['Moz', 'Webkit', 'Khtml', 'O', 'ms'],
        p = p.charAt(0).toUpperCase() + p.substr(1);
        for(var i=0; i<v.length; i++) {
          if(typeof s[v[i] + p] == 'string') { return true; }
        }
        return false;
    },

    css: function(element, property, value)
    {
        if(value)
        {
            element.css('-ms-' + property, value);
            element.css('-moz-' + property, value);
            element.css('-webkit-' + property, value);
            element.css('-o-' + property, value);
        }
        else
        {
            var value1 = element.css('-ms-' + property);
            var value2 = element.css('-moz-' + property);
            var value3 = element.css('-webkit-' + property);
            var value4 = element.css('-o-' + property);

            return value1 || value2 || value3 || value4;
        }
    },

    getCssAnimationEffect: function(uiEffect)
    {
        switch(uiEffect)
        {
            case 'linear':
                return 'linear';

            case 'swing':
            case 'easeOutSine':
            case 'easeOutExpo':
            case 'easeOutCirc':
            case 'easeOutCubic':
            case 'easeOutQuad':
            case 'easeOutQuart':
            case 'easeOutQuint':
                return 'ease-out';

            case 'easeInSine':
            case 'easeInExpo':
            case 'easeInCirc':
            case 'easeInCubic':
            case 'easeInQuad':
            case 'easeInQuart':
            case 'easeInQuint':
                return 'ease-in';

            case 'easeInOutSine':
            case 'easeInOutExpo':
            case 'easeInOutCirc':
            case 'easeInOutCubic':
            case 'easeInOutQuad':
            case 'easeInOutQuart':
            case 'easeInOutQuint':
                return 'ease-in-out';

            default: return 'default';
        }

    }

});
})(jQuery);

//Slider_Horizontal class
function Slider_Horizontal()
{
    this.sliderTypeClass = 'horizontal';
    this.allowVerticalWipeTouch = false;
    this.allowHorizontalWipeTouch = true;
    this.allowVerticalKeys = false;
    this.allowHorizontalKeys = true;
}

(function($) {

//Slider_Horizontal class methods
$.extend(Slider_Horizontal.prototype, Slider_Abstract.prototype,
{
    /**
     * adjust pane width
     */
    initPaneDimensions: function()
    {
        var width = 0;
        if(this.items.length > 0)
        {
            width += $(this.items[0]).outerWidth(true);
        }
        if(this.items.length > 1)
        {
            width += $(this.items[this.items.length - 1]).outerWidth(true);
        }
        if(this.items.length > 2)
        {
            width += this.itemDimension * (this.items.length - 2);
        }
        this.pane.width(width);

        this.items.css('float', 'left');
    },

    /**
     * adjust slider height
     */
    initSliderDimensions: function()
    {
        //zrusi sa vyska
//        this.slider.attr('style', this.slider.attr('style').toLowerCase().replace(/width[^;]+;?/, ''));
//        if(this.slider.width() > this.pane.width())
//        {
//            this.slider.width(this.pane.width());
//        }
    },

    /**
     * get item dimension
     */
    getItemDimension: function()
    {
        return $(this.items[1]).outerWidth(true);
    },

    /**
     * get item position
     */
    getItemPosition: function(item)
    {
        return item.position().left;
    },

    /**
     * get pane position
     */
    getPanePosition: function()
    {
        if(this.cssAnimationSupport)
        {
            var matrix = this.css(this.pane, 'transform');
            if(matrix && matrix != 'none')
            {
                var match = matrix.match(/matrix\((.+?),\s*(.+?),\s*(.+?),\s*(.+?),\s*(.+?),\s*(.+?)\)/);
                if(match)
                {
                    return parseInt(match[5].replace('px', ''));
                }
            }
            return 0;
        }
        return parseInt(this.pane.css('left'));
    },

    /**
     * set pane dimension
     */
    setPanePosition: function(left)
    {
        if(this.cssAnimationSupport)
        {
            this.css(this.pane, "transition", "none");
            this.css(this.pane, "transform", "translate3d(" + left + "px, 0px, 0px)");

        }
        else
        {
            this.pane.css('left', left);
        }
    },

    /**
     * get pane dimension
     */
    getPaneDimension: function()
    {
        return this.pane.width();
    },

    /**
     * get slider dimension
     */
    getSliderDimension: function()
    {
        return this.slider.width();
    },

    /**
     * get element margin
     */
    getElementMargin: function(element)
    {
        return parseInt(element.css('margin-left'));
    },

    /**
     * get element position
     */
    getElementPosition: function(element)
    {
        return Math.round(element.position().left / this.scaleObj.data('scale').factor);
    },

    /**
     * animate
     */
    animate: function(move, settings)
    {
        var defaultSettings = {
            effect: this.settings.easingEffect,
            speed: this.settings.speed,
            callback: null
        };
        var mergedSettings = $.extend({}, defaultSettings, settings || {});

        if(this.settings.disableWipeTouchAnimation)
        {
            this.setPanePosition(move);
            this.animationEnd(mergedSettings.callback)
            return;
        }

        this.startAnimation();

        if(this.cssAnimationSupport)
        {
            //end events
            this.pane.css("-webkit-backface-visibility","hidden");
            this.pane.css("-webkit-perspective","1000");

            this.pane.bind('webkitTransitionEnd', $.proxy( function() { this.animationEnd(mergedSettings.callback) }, this));
            this.pane.bind('transitionend', $.proxy( function() { this.animationEnd(mergedSettings.callback) }, this));
            this.pane.bind('MSTransitionEnd', $.proxy( function() { this.animationEnd(mergedSettings.callback) }, this));
            this.pane.bind('oTransitionEnd', $.proxy( function() { this.animationEnd(mergedSettings.callback) }, this));

            this.css(this.pane, "transition-property", "all");
            this.css(this.pane, "transition-duration", (mergedSettings.speed / 1000) + 's');
            this.css(this.pane, "transition-timing-function", this.getCssAnimationEffect(mergedSettings.effect));
            this.css(this.pane, "transform", "translate3d(" + move + "px, 0px, 0px)");
        }
        else
        {
            this.pane.animate({left: move}, mergedSettings.speed, mergedSettings.effect, $.proxy( function() { this.animationEnd(mergedSettings.callback) }, this));
        }
    }
});
})(jQuery);

/**
 * jQuery - Slider plugin
 */
(function($)
{
    var types = {
        horizontal: 1,
        vertical: 2
    };

    var methods = {
        init: function(type, settings)
        {
            var ret;
            this.each(function()
            {
                var $this = $(this);
                var slider = $this.data('slider');
                if (!slider)
                {
                    if(type == types.vertical)
                    {
                        slider = new Slider_Vertical();
                    }
                    else
                    {
                        slider = new Slider_Horizontal();
                    }
                }
                var mergedSettings = $.extend({}, $.fn.slidable.defaults, settings || {});
                slider.init($this, mergedSettings);
                $this.data('slider', slider);
            });
            return ret;
        },

        get:function()
        {
            var slider = this.data('slider');
            if (!slider)  $.error('Method get must be call after init on jQuery.slider');
            return slider;
        }
    };

    $.extend($.fn,
    {
        slidable: function(type, method)
        {
            if (methods[method])
            {
                return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 2));
            }
            else if (typeof method === 'object' || ! method)
            {
//                Array.prototype.unshift.call(arguments, type);
                return methods.init.apply(this,  arguments);
            }
            else
            {
                $.error('Method ' + method + ' does not exist on jQuery.slider');
            }
        },

        verticalSlider:function(method)
        {
            return this.slidable(types.vertical, method);
        },

        horizontalSlider:function(method)
        {
            return this.slidable(types.horizontal, method);
        }
    });

    //plugin defaults settings
    $.fn.slidable.defaults = {
        speed: 400,
        itemsToMove: 1, //o kolko itemov sa ma posuvat slider
        fullSizeItem: false,
        autoSelect: true,
        createNavigation: true, //ci sa maju zobrazit sipky
        mouseWheelContainer: null,
        enableKeys: false,
        easingEffect: 'linear',
        cssAnimation: false, //ak je dostupna css animacia tak sa pouzije
        wipePreventDefault: true,
        wipeTouchActive: true,
        wipeTouchItemsToMove: 1, //ak je null vyrata sa z rychlosti prsta
        wipeTouchEasingSpeed: 200,
        wipeTouchEasingEffect: 'linear',
        wipeTouchMinMove: 100, //min pohyb prstom v px na ktory sa reaguje
        wipeTouchElasticLength:0,
        wipeTouchItemTreshold: 1, //percenta zo sirky itemu, o ktore ked sa posunie slider tak sa uz ide na dalsi prvok
        wipeLeftCallback: null, //callback ktory sa vykona pri pohybe prstom dolava
        wipeRightCallback: null, //callback ktory sa vykona pri pohybe prstom doprava
        wipeTouchEnableMoveWhileAnimation: true //ci je mozne hybat slider pocas animacie
    };
})(jQuery);


//WipeTouch class
function WipeTouch()
{
    this.element = null;

    this.active = true;
    this.startX = false; // where touch has started, left
    this.startY = false; // where touch has started, top
    this.startDate = false; // used to calculate timing and aprox. acceleration
    this.curX = false; // keeps touch X position while moving on the screen
    this.curY = false; // keeps touch Y position while moving on the screen
    this.isMoving = false; // is user touching and moving?
    this.touchedElement = false; // element which user has touched
}

(function($) {

//WipeTouch class methods
$.extend(WipeTouch.prototype,
{
    /**
     * init
     */
    init: function($element, settings)
    {
        this.element = $element;
        this.settings = $.extend({}, $.fn.wipetouch.defaults, settings || {});

        this.clear();
        this.resetTouch();
        if ('ontouchstart' in document.documentElement)
        {
            this.element[0].addEventListener('touchstart', $.proxy(this.onTouchStart, this), false);
            this.element[0].addEventListener('touchend', $.proxy(this.onTouchEnd, this), false);
            this.element[0].addEventListener('touchmove', $.proxy( this.onTouchMove, this) , false);
        }
        this.active = true;
    },

    bindEvents: function()
    {

    },

    clear: function()
	{
        if ('ontouchstart' in document.documentElement)
        {
            this.element[0].removeEventListener('touchstart', $.proxy(this.onTouchStart, this), false);
            this.element[0].removeEventListener('touchend', $.proxy(this.onTouchEnd, this), false);
            this.element[0].removeEventListener('touchmove', $.proxy( this.onTouchMove, this) , false);
        }
    },

    // Called when user touches the screen
    onTouchStart: function(e)
    {
        if(!this.active) return;

        if (!this.isMoving && e.touches.length > 0)
        {
//            e.preventDefault();

            this.startDate = new Date().getTime();

            this.startX = e.touches[0].pageX;
            this.startY = e.touches[0].pageY;
            this.curX = this.startX;
            this.curY = this.startY;
            this.isMoving = true;

            this.touchedElement = $(e.target);
        }

    },

    // Called when user untouches the screen
    onTouchEnd: function(e)
    {
        if(!this.active) return;
        if(this.isMoving)
        {
            this.touchCalculate(e);
        }
    },

    // Called when user is touching and moving on the screen
    onTouchMove: function(e)
    {
//        e.preventDefault();
        if(!this.active) return;

        if (this.isMoving)
        {
            var result = {};
            result.deltaX = e.touches[0].pageX - this.curX;
            result.deltaY = e.touches[0].pageY - this.curY;

            this.curX = e.touches[0].pageX;
            this.curY = e.touches[0].pageY;
            if(Math.abs(result.deltaX) > Math.abs(result.deltaY))
            {
                e.preventDefault();
//                if (this.settings.preventDefault)
//                {
//                    e.preventDefault();
//                }
//                this.triggerEvent(this.settings.wipeMove, result);
            }
//            else if(Math.abs(result.deltaY) > 10 && this.settings.allowVertical)
            {
//                e.preventDefault();
//                if (this.settings.preventDefault)
//                {
//                    e.preventDefault();
//                }
//                this.triggerEvent(this.settings.wipeMove, result);
            }
        }
    },

    // ------------------------------------------------------------------------
    // CALCULATE TOUCH AND TRIGGER
    // ------------------------------------------------------------------------
    touchCalculate: function(e)
    {
        var endDate = new Date().getTime();	// current date to calculate timing
        var ms = this.startDate - endDate; // duration of touch in milliseconds

        var x = this.curX;			// current left position
        var y = this.curY;			// current top position
        var dx = x - this.startX;	// diff of current left to starting left
        var dy = y - this.startY;	// diff of current top to starting top
        var ax = Math.abs(dx);	// amount of horizontal movement
        var ay = Math.abs(dy);	// amount of vertical movement

        // moved less than 15 pixels and touch duration less than 100ms,
        // if tapToClick is true then triggers a click and stop processing
//        if (this.settings.tapToClick && ax <  this.settings.moveX && ay <  this.settings.moveY && ms < 100)
//        {
//            if(this.touchedElement[0].nodeName == '#text')
//            {
//                this.touchedElement = this.touchedElement.parent();
//            }
//
//            this.touchedElement.trigger("click");
//            this.resetTouch();
//            return;
//        }

        var toright = dx > 0;	// if true X movement is to the right, if false is to the left
        var tobottom = dy > 0;	// if true Y movement is to the bottom, if false is to the top

        // calculate speed from 1 to 5, being 1 slower and 5 faster
        var s = ((ax + ay) * 60) / ((ms) / 15 * (ms));

        if (s < 1) s = 1;
        if (s > 10) s = 10;

        var result = {speed: parseInt(s), x: ax, y: ay};
        if (ax >= this.settings.moveX && ay <= this.settings.moveY && ax > ay)
        {
            if (toright)
            {
                this.triggerEvent(this.settings.wipeRight, result);
            }
            else
            {
                this.triggerEvent(this.settings.wipeLeft, result);
            }

        }


        this.resetTouch();
    },

    // Resets the cached variables
    resetTouch: function()
    {
        this.startX = false;
        this.startY = false;
        this.startDate = false;
        this.isMoving = false;
    },

    start: function()
    {
        this.resetTouch();
        this.active = true;
    },

    stop: function()
    {
        this.active = false;
        this.resetTouch();
    },

    // Triggers a wipe event passing a result object with
    // speed from 1 to 5, and x / y movement amount in pixels
    triggerEvent: function(wipeEvent, result)
    {
        if (wipeEvent) wipeEvent(result);
    }
});
})(jQuery);


(function($)
{
    var methods = {
        init: function(settings)
        {
            var ret;
            this.each(function()
            {
                var $this = $(this);
                var wipetouch = $this.data('wipetouch');
                if (!wipetouch)
                {
                    wipetouch = new WipeTouch();
				}
                wipetouch.init($this, settings);
                $this.data('wipetouch', wipetouch);
				ret = ret ? ret.add($this) : $this;
            });
            return ret;
        },

        get:function()
        {
            var wipetouch = this.data('wipetouch');
            if (!wipetouch)  $.error('Method get must be call after init on jQuery.wipetouch');
            return wipetouch;
        }
    };

    $.extend($.fn,
    {
        wipetouch: function(method)
        {
            if (methods[method])
            {
                return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
            }
            else if (typeof method === 'object' || ! method)
            {
                return methods.init.apply(this, arguments);
            }
            else
            {
                $.error('Method ' + method + ' does not exist on jQuery.wipetouch');
            }
        }
    });

    //plugin defaults settings
    $.fn.wipetouch.defaults = {
        moveX:				50,		// minimum amount of horizontal pixels to trigger a wipe event
        moveY:				50,		// minimum amount of vertical pixels to trigger a wipe event
        preventDefault:		true,	// if true, prevents default events (click for example)
        allowDiagonal:		false,	// if false, will trigger horizontal and vertical movements so
        allowVertical:		true,	// if false, will trigger horizontal movements
        allowHorizontal:	false,	// if false, will trigger vertical movements
        tapToClick:			false,	// if user taps the screen it will fire a click event on the touched element
        wipeLeft:			false,	// called on wipe left gesture
        wipeRight:			false,	// called on wipe right gesture
        wipeUp:				false,	// called on wipe up gesture
        wipeDown:			false,	// called on wipe down gesture
        wipeUpLeft:			false,	// called on wipe top and left gesture
        wipeDownLeft:		false,	// called on wipe bottom and left gesture
        wipeUpRight:		false,	// called on wipe top and right gesture
        wipeDownRight:		false,	// called on wipe bottom and right gesture
        wipeMove: false // called on wipe move
    };
})(jQuery);