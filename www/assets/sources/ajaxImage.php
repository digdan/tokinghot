<?php
require_once("../includes/core.php");
$data = array();
$user = $sm['user']['id'];
$site_base = $config['site_url'];
$photos_base = "/assets/sources/uploads/photos/";
if( isset( $_POST ) && !empty( $_FILES['photos'] )){
	//get the structured array
	$images = restructure_array(  $_FILES );
	$allowedExts = array("gif", "jpeg", "jpg", "png");

	foreach ( $images as $key => $value){
		$i = $key+1;
		$image_name = $value['name'];
		//get image extension
		$ext = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));
		//assign unique name to image
		$name = $i*time().'.'.$ext;
		//$name = $image_name;
		//image size calcuation in KB
		$image_size = $value["size"] / 1024;
		$image_flag = true;
		//max image size
		$max_size = 2048;
		if( in_array($ext, $allowedExts) && $image_size < $max_size ){
			$image_flag = true;
		} else {
			$image_flag = false;
			$data[$i]['error'] = $image_name. ' exceeds max '.$max_size.' KB size or incorrect file extension';
		} 
		
		if( $value["error"] > 0 ){
			$image_flag = false;
			$data[$i]['error'] = '';
			$data[$i]['error'].= '<br/> '.$image_name.' Image contains error - Error Code : '.$value["error"];
		}
		
		if($image_flag){
			move_uploaded_file($value["tmp_name"], "uploads/photos/".$name);
			$src = "uploads/photos/".$name;
			$dist = "uploads/photos/thumbnail_".$name;
			$dist2 = "uploads/photos/photo_".$name;			
			$data[$i]['success'] = $thumbnail = 'thumbnail_'.$name;
			$data[$i]['success'] = $photo = 'photo_'.$name;			
			thumbnail($src, $dist, 200);
			photo($src, $dist2);			
			$u_foto = $mysqli->query("SELECT * FROM usuarios_fotos where u_id = '".$user."' and perfil = 1");
			if($sm['config']['add_watermark'] == 1){
				$ins_photo = $site_base.$photos_base.$photo;
			} else {
				$ins_photo = $site_base.$photos_base.$name;
			}
			$ins_thumb = $site_base.$photos_base.$thumbnail;
			if ($u_foto->num_rows == 0) 
			{	
				$mysqli->query("INSERT INTO usuarios_fotos(u_id,foto,thumb,perfil,aprovada) 
														   VALUES('$user','$ins_photo', '$ins_thumb',1,'".$sm['config']['profile_photo_review']."')");
				$mysqli->query("UPDATE usuarios set ready = 2 where id = '".$user."'");				
			}else{
				$mysqli->query("INSERT INTO usuarios_fotos(u_id,foto,thumb,aprovada) 
														   VALUES ('$user','$ins_photo', '$ins_thumb','".$sm['config']['photo_review']."')");	
			}			
		}
	}
	echo json_encode($data);
	
} else {
	$data[] = 'No Image Selected..';
}



function restructure_array(array $images)
{
	$result = array();

	foreach ($images as $key => $value) {
		foreach ($value as $k => $val) {
			for ($i = 0; $i < count($val); $i++) {
				$result[$i][$k] = $val[$i];
			}
		}
	}

	return $result;
}



function thumbnail($src, $dist, $dis_width = 100 ){

	$img = '';
	$extension = strtolower(strrchr($src, '.'));
	switch($extension)
	{
		case '.jpg':
		case '.jpeg':
			$img = @imagecreatefromjpeg($src);
			break;
		case '.gif':
			$img = @imagecreatefromgif($src);
			break;
		case '.png':
			$img = @imagecreatefrompng($src);
			break;
	}
	$width = imagesx($img);
	$height = imagesy($img);




	$dis_height = $dis_width * ($height / $width);

	$new_image = imagecreatetruecolor($dis_width, $dis_height);
	imagecopyresampled($new_image, $img, 0, 0, 0, 0, $dis_width, $dis_height, $width, $height);


	$imageQuality = 100;

	switch($extension)
	{
		case '.jpg':
		case '.jpeg':
			if (imagetypes() & IMG_JPG) {
				imagejpeg($new_image, $dist, $imageQuality);
			}
			break;

		case '.gif':
			if (imagetypes() & IMG_GIF) {
				imagegif($new_image, $dist);
			}
			break;

		case '.png':
			$scaleQuality = round(($imageQuality/100) * 9);
			$invertScaleQuality = 9 - $scaleQuality;

			if (imagetypes() & IMG_PNG) {
				imagepng($new_image, $dist, $invertScaleQuality);
			}
			break;
	}
	imagedestroy($new_image);
}

function photo($src, $dist){

	$img = '';
	$extension = strtolower(strrchr($src, '.'));
	switch($extension)
	{
		case '.jpg':
		case '.jpeg':
			$img = @imagecreatefromjpeg($src);
			break;
		case '.gif':
			$img = @imagecreatefromgif($src);
			break;
		case '.png':
			$img = @imagecreatefrompng($src);
			break;
	}
	$width = imagesx($img);
	$height = imagesy($img);

	$stamp = imagecreatefrompng('../../themes/sexymetro/img/watermark.png');	
	$marge_right = 10;
	$marge_bottom = 10;
	$sx = imagesx($stamp);
	$sy = imagesy($stamp);	
	
	$new_image = imagecreatetruecolor($width, $height);
	imagecopy($img, $stamp, imagesx($img) - $sx - $marge_right, imagesy($img) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));	

	$imageQuality = 100;

	switch($extension)
	{
		case '.jpg':
		case '.jpeg':
			if (imagetypes() & IMG_JPG) {
				imagejpeg($img, $dist, $imageQuality);
			}
			break;

		case '.gif':
			if (imagetypes() & IMG_GIF) {
				imagegif($img, $dist);
			}
			break;

		case '.png':
			$scaleQuality = round(($imageQuality/100) * 9);
			$invertScaleQuality = 9 - $scaleQuality;

			if (imagetypes() & IMG_PNG) {
				imagepng($img, $dist, $invertScaleQuality);
			}
			break;
	}

	imagedestroy($img);
}