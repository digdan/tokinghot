<?php
if ($logged !== true) {
    header('Location: ' . smoothLink('index.php?page=preview'));
}

if ($mobile === true){
$sm['content'] = getMobilePage('profile/content');	
}
else {
$sm['content'] = getPage('profile/content');
}
