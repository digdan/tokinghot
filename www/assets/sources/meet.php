<?php
if ($logged !== true) {
    header('Location: ' . smoothLink('index.php?page=preview'));
}

$sm['content'] = getPage('meet/content');
