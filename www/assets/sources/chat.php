<?php
if ($logged !== true) {
    header('Location: ' . smoothLink('index.php?page=preview'));
}

if ($mobile === true){
$sm['content'] = getMobilePage('chat/content');	
}
else {
$sm['content'] = getPage('chat/content');
}
