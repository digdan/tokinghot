<?php
require_once("assets/includes/core.php");
require_once("assets/social/facebook/facebook.php");

$facebook = new Facebook(array(
  'appId'  => $fb_app_id,   // Facebook App ID 
  'secret' => $fb_app_secret,  // Facebook App Secret
  'cookie' => true,	
));
$fbuser = $facebook->getUser();

if ($fbuser) {
  try {  
    $user_profile = $facebook->api('/me?fields=name,email', 'GET');
  	    $fbid = $user_profile['id'];                 // To Get Facebook ID
 	    $fbuname = $user_profile['username'];  // To Get Facebook Username
 	    $fbfullname = $user_profile['name']; // To Get Facebook full name
	    $femail = $user_profile['email']; 		
        checkuser($fbid,$fbfullname,$femail);    // INSERT NEW USER IN DB
  } catch (FacebookApiException $e) {
    error_log($e);
   $fbuser = null;
  }
}
if ($fbuser) {
	if($mobile === true) {
		header('Location: ' . smoothLink('mobile.php?page=settings'));
	} else {
		header('Location: ' . smoothLink('index.php?page=settings'));
	}
} else {
	$loginUrl = $facebook->getLoginUrl(array(
		'scope'		=> 'email', // Permissions to request from the user
		));
	header("Location: ".$loginUrl);
}

