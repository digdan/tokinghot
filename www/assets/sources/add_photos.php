<?php
if ($logged !== true) {
    header('Location: ' . smoothLink('index.php?page=preview'));
}

if ($mobile === true){
$sm['content'] = getMobilePage('user/add_photos');	
}
else {
$sm['content'] = getPage('user/add_photos');
}
