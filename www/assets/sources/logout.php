<?php
if (isset($_SESSION['user'])) {
    unset($_SESSION['user']);
}
header('Location: ' . smoothLink('index.php?page=preview'));
