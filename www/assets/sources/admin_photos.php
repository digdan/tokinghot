<?php
if ($logged !== true) {
    header('Location: ' . smoothLink('index.php?page=preview'));
}
if ($sm['user']['admin'] != 1) {
    header('Location: ' . smoothLink('index.php?page=game'));
}

$sm['content'] = getAdminPage('admin/review_photos');
