<?php
if ($logged !== true) {
    header('Location: ' . smoothLink('index.php?page=preview'));
}

if ($mobile === true){
$sm['content'] = getMobilePage('user/settings');	
}
else {
$sm['content'] = getPage('user/settings');
}