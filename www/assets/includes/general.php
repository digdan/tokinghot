<?php
/*	SITE NAME	*/
$config['site_name'] = 'Toking Hot';

/*	SITE TITLE	*/
$config['site_title'] = 'Toking Hot - Where toking hot users can upload their photos and meet new people';

/*	SITE EMAIL	*/
$config['site_mail'] = 'info@tokinghot.com';

/*	SITE DEFAULT LANGUAGE
	en = English
	es = Spanish
	fr = French
	it = Italian
	pt = Portuguese
	de = Deutsch	*/
$config['language'] = 'en';

/*	REVIEW PHOTOS
	0 = Photos need to be approved by an admin of your site before display to other users
	1 = Photos will be auto approved	*/
$config['photo_review'] = 1;

/*	REVIEW PROFILE PHOTOS
	0 = Profile photo need to be approved by an admin of your site before display to other users
	1 = Profile Photo will be auto approved
	
	Tip: Porfile photo by default is the first uploaded photo by the user;	*/
$config['profile_photo_review'] = 1;

/*	ADD WATERMARKS TO UPLOADED PHOTOS 
	0 = No watermark the photos
	1 = Add watermark (watermark.png file is located in themes/sexymetro/img/ ) you can edit it to your logo or whatever you want	*/
$config['add_watermark'] = 1; 

/*	EMAIL NOTIFICATION WHEN USER CLICK YES IN SEXY GAME 
	0 = Do not send mail notification
	1 = Send mail notification
	
	Tip: Sending mail notification may slow a little bit the speed of load new user in sexy game*/
$config['sexy_mail_notification'] = 1;

/*	EMAIL NOTIFICATION WHEN USER SEND MESSAGE AND THE OTHER USER IS OFFLINE
	0 = Do not send mail notification
	1 = Send mail notification	*/
$config['chat_mail_notification'] = 1;