<?php
date_default_timezone_set('America/Chicago');
session_cache_limiter('none');
session_start();
header('Content-Type: text/html; charset=ISO-8859-1');
// Include 'config.php' file
require('config.php');
require 'mail/PHPMailerAutoload.php';

// Connect to SQL Server
$mysqli = new mysqli($db_host, $db_username, $db_password,$db_name);

// Check connection
if (mysqli_connect_errno($mysqli)) {
    exit(mysqli_connect_error());
}

// Fetch site configurations
require('general.php');
require('theme.php');

$config['site_url'] = $site_url;
$config['theme_url'] = $site_url . '/themes/' . $config['theme'];
$config['theme_url_mobile'] = $site_url . '/themes/' . $config['theme_mobile'];
$config['theme_url_email'] = $site_url . '/themes/' . $config['theme_email'];
$config['ajax_path'] = $site_url . '/requests';
require('ads.php');

if (!isset($_SESSION['lang'])) {
    $_SESSION['lang'] = $config['language'];
}

// Stores site configurations to variables for later use
$sm = array();
$sm['config'] = $config;
$mobile = false;

// Login verification and user stats update
$logged = false;
$user = array();

if (!empty($_SESSION['user']) && is_numeric($_SESSION['user']) && $_SESSION['user'] > 0) {
	$user_id = secureEncode($_SESSION['user']);
	$logged = true;
	getUserInfo($user_id,0);      
}

$sm['logged'] = $logged;

if (!empty($_GET['lang'])) {
	
    if (file_exists('./assets/includes/languages/' . $_GET['lang'] . '.php')) {
        $config['language'] = $_GET['lang'];
        $_SESSION['lang'] = $_GET['lang'];
        if ($logged == true) {
           $mysqli->query("UPDATE usuarios SET lang = '".$_GET['lang']."' WHERE id = '".$user_id."'"); 
        }
    }
}

require_once('languages/' . $_SESSION['lang'] . '.php');

// Removes session and unnecessary variables if user verification fails
if ($logged == false) {
    unset($_SESSION['user']);
    unset($user);
}