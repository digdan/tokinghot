SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `chat`
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `s_id` int(9) NOT NULL,
  `r_id` int(9) NOT NULL,
  `mensaje` varchar(350) NOT NULL,
  `fecha` varchar(100) NOT NULL,
  `visto` int(11) NOT NULL DEFAULT '0',
  `enviado` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=867 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chat
-- ----------------------------

-- ----------------------------
-- Table structure for `notificaciones`
-- ----------------------------
DROP TABLE IF EXISTS `notificaciones`;
CREATE TABLE `notificaciones` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `u_id` int(9) NOT NULL,
  `s_id` int(9) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT '0',
  `visto` int(1) NOT NULL DEFAULT '0',
  `fecha` varchar(100) NOT NULL,
  `popup` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2002 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notificaciones
-- ----------------------------

-- ----------------------------
-- Table structure for `sexy`
-- ----------------------------
DROP TABLE IF EXISTS `sexy`;
CREATE TABLE `sexy` (
  `u1` int(11) NOT NULL,
  `u2` int(11) NOT NULL,
  `sexy` int(1) NOT NULL,
  PRIMARY KEY (`u1`,`u2`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sexy
-- ----------------------------

-- ----------------------------
-- Table structure for `usuarios`
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `edad` varchar(9) NOT NULL DEFAULT '',
  `sexo` int(1) NOT NULL,
  `loc` varchar(150) NOT NULL,
  `bio` varchar(300) NOT NULL,
  `sexy` int(9) NOT NULL DEFAULT '0',
  `total` int(9) NOT NULL DEFAULT '0',
  `pattern` int(9) NOT NULL DEFAULT '0',
  `chat` int(9) NOT NULL DEFAULT '0',
  `likes` int(9) NOT NULL DEFAULT '0',
  `looking` int(1) NOT NULL DEFAULT '0',
  `ready` int(1) NOT NULL DEFAULT '0',
  `lat` varchar(50) NOT NULL DEFAULT '0',
  `lng` varchar(50) NOT NULL DEFAULT '0',
  `id_facebook` bigint(50) NOT NULL DEFAULT '0',
  `last_access` varchar(100) NOT NULL,
  `admin` int(1) NOT NULL DEFAULT '0',
  `lang` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=187 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('186', 'admin', 'admin@sexymetro.com', '$1$I8FFg2eB$r/KCknEdSXSZCCaxwEF9o.', '24', '1', 'Múnich, Alemania', 'admin', '0', '0', '0', '0', '0', '0', '1', '48.1351253', '11.581980599999952', '0', '1415289857', '1', 'en');

-- ----------------------------
-- Table structure for `usuarios_fotos`
-- ----------------------------
DROP TABLE IF EXISTS `usuarios_fotos`;
CREATE TABLE `usuarios_fotos` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `u_id` int(9) NOT NULL,
  `foto` varchar(300) NOT NULL,
  `thumb` varchar(300) NOT NULL,
  `perfil` int(1) NOT NULL DEFAULT '0',
  `aprovada` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=811 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios_fotos
-- ----------------------------

-- ----------------------------
-- Table structure for `usuarios_socials`
-- ----------------------------
DROP TABLE IF EXISTS `usuarios_socials`;
CREATE TABLE `usuarios_socials` (
  `u_id` int(9) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `skype` varchar(100) NOT NULL,
  `whatsapp` varchar(100) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios_socials
-- ----------------------------
INSERT INTO `usuarios_socials` VALUES ('186', '', '', '', '');
