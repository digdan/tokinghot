<?php

// Sexymetro - A Hot or Not Platform

require_once('connect.php');

function sexymetro($uid)	
{	
	global $mysqli;
	$f_user = $mysqli->query("SELECT sexy,total FROM usuarios where id = '".$uid."'");	
	$f_us = $f_user->fetch_object();
	
	if($f_us->total >= 20) {
		$porcentaje = "0";
		$porcentaje = $f_us->sexy  / $f_us->total * 100; 
		$porcentaje = round($porcentaje);
		
		if($porcentaje < 10 ) {
			$porcentaje = "0".$porcentaje;						
		}
		
		$mostrar_porcentaje = substr($porcentaje, 0,1).".".substr($porcentaje, 1,1);				
	} else {
		$porcentaje = "0";
		$mostrar_porcentaje = "???";
	}
	
	$sexymetro['percent'] = $porcentaje;
	$sexymetro['show'] = $mostrar_porcentaje;	
	
	return $sexymetro;	
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

//GET DISTANCE IN KM
function distance($lat1, $lon1, $lat2, $lon2)
{
  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $kilometer = $miles * 1.609344;
  $kilometer = round($kilometer);
  return $kilometer; 
}

//CHECK USER STATUS
function status($uid, $size)
{
	global $mysqli;
	$f_user = $mysqli->query("SELECT last_access FROM usuarios where id = '".$uid."'");	
	$f_us = $f_user->fetch_object();	
	if ($f_us->last_access+300 >= time()) {
		$status = '<a class="hint--bottom hint--success" data-hint="Online" href="javascript:void(0)">
		<i class="fa fa-circle" style="color:#19a20a; font-size:'.$size.'px;"></i></a>';
	} else {
		$status = '<a class="hint--bottom" data-hint="Offline" href="javascript:void(0)">
		<i class="fa fa-circle" style="color:#333; font-size:'.$size.'px;"></i></a>';
	}
	return $status;
}

function profilePhoto($uid) {
	global $mysqli,$sm;
	
	$uid = secureEncode($uid);
	$photo = $mysqli->query("SELECT thumb FROM usuarios_fotos where u_id = '".$uid."' and perfil = 1 and aprovada = 1");
	if($photo->num_rows == 1) {
		$profile = $photo->fetch_object();
		$profile_photo = $profile->thumb;
	} else {
		$profile_photo = $sm['config']['theme_url']."/img/no_user.jpg";
	}
	return $profile_photo;
}

function getUserPhotos($uid,$admin) {
	global $mysqli;
	$photos = '';
	
	if($uid == "aproved"){
		$u_foto = $mysqli->query("SELECT * FROM usuarios_fotos where aprovada = 1 order by id DESC");
	} else if($uid == "waiting"){
		$u_foto = $mysqli->query("SELECT * FROM usuarios_fotos where aprovada = 0 order by id DESC");
	} else if($uid == "unaproved"){
		$u_foto = $mysqli->query("SELECT * FROM usuarios_fotos where aprovada = 2 order by id DESC");
	} else if($uid == "all"){
		$u_foto = $mysqli->query("SELECT * FROM usuarios_fotos order by id DESC");
	} else{
		$u_foto = $mysqli->query("SELECT * FROM usuarios_fotos where u_id = '".$uid."' order by id DESC");
	}
	
	if ($u_foto->num_rows > 0) { 
		if($admin == 1){
			while($u_fotos = $u_foto->fetch_object()){
				 $photos .= getAdminPhoto($u_fotos->id);
			}
		} else {
			while($u_fotos = $u_foto->fetch_object()){
				 $photos .= getUserPhoto($u_fotos->id);
			}				
		}
	}
	return $photos;			
}

function getUserPhotosMobile($uid) {
	global $mysqli;
	$photos = '';
	$uid = secureEncode($uid);
	$u_foto = $mysqli->query("SELECT * FROM usuarios_fotos where u_id = '".$uid."' order by id DESC");

	if ($u_foto->num_rows > 0) { 
		while($u_fotos = $u_foto->fetch_object()){
				 $photos .= getUserPhotoMobile($u_fotos->id);
		}				
	}
	return $photos;			
}

function manageUsers($start,$limit) {
	global $mysqli,$sm;
	$mUsers = '';
	$start = secureEncode($start);
	$limit = secureEncode($limit);	
	$query = $mysqli->query("SELECT id,email,nombre FROM usuarios ORDER BY id desc LIMIT $start,$limit");

	if ($query->num_rows > 0) { 
		while($users = $query->fetch_object()){
			$sm['mUser']['id'] = $users->id;
			$sm['mUser']['photo'] = profilePhoto($users->id);
			$sm['mUser']['name'] = $users->nombre;
			$sm['mUser']['email'] = $users->email;			
			$mUsers .= getPage('admin/manage_users');
		}				
	}
	return $mUsers;			
}

function findUsers($q) {
	global $mysqli,$sm;
	$mUsers = '';
	$q = secureEncode($q);	
	$query = $mysqli->query("SELECT id,email,nombre FROM usuarios where id = '".$q."' OR email = '".$q."' OR nombre LIKE '%$q%'");

	if ($query->num_rows > 0) { 
		while($users = $query->fetch_object()){
			$sm['mUser']['id'] = $users->id;
			$sm['mUser']['photo'] = profilePhoto($users->id);
			$sm['mUser']['name'] = $users->nombre;
			$sm['mUser']['email'] = $users->email;
			
			$mUsers .= getPage('admin/manage_users');
		}				
	}
	return $mUsers;			
}

function findUsersByGender($q) {
	global $mysqli,$sm;
	$mUsers = '';
	$q = secureEncode($q);	
	$query = $mysqli->query("SELECT id,email,nombre FROM usuarios where sexo = '".$q."'");

	if ($query->num_rows > 0) { 
		while($users = $query->fetch_object()){
			$sm['mUser']['id'] = $users->id;
			$sm['mUser']['photo'] = profilePhoto($users->id);
			$sm['mUser']['name'] = $users->nombre;
			$sm['mUser']['email'] = $users->email;
			
			$mUsers .= getPage('admin/manage_users');
		}				
	}
	return $mUsers;			
}

function findUsersOnline() {
	global $mysqli,$sm;
	$mUsers = '';
	$time_now = time()-300;
	$query = $mysqli->query("SELECT id,email,nombre FROM usuarios WHERE last_access >= '".$time_now."'");

	if ($query->num_rows > 0) { 
		while($users = $query->fetch_object()){
			$sm['mUser']['id'] = $users->id;
			$sm['mUser']['photo'] = profilePhoto($users->id);
			$sm['mUser']['name'] = $users->nombre;
			$sm['mUser']['email'] = $users->email;
			
			$mUsers .= getPage('admin/manage_users');
		}				
	}
	return $mUsers;			
}

function getUserCrushes($uid) {
	global $mysqli,$lang;
	$crushes = '';
	$likes = $mysqli->query("SELECT u1 FROM sexy where u2 = '".$uid."' and sexy = 1");
	
	if ($likes->num_rows > 0) {
		$crushes = '<center><h4 style="color:#dfdfdf;">'.$lang['people_saying_u_r_sexy'].'</h4></center><ul class="gallery square-thumb">';
		while($crush = $likes->fetch_object()){
			 $crushes .= getCrushes($crush->u1);
		}
		$crushes .= '</ul>';
	} else {
		$crushes = '<center><h4 style="color:#dfdfdf;">'.$lang['noone_say_you_are_sexy'].'</h4></center>';
	}
	return $crushes;			
}

function getCrushes($cid) {
	global $mysqli,$sm;
	$crushes = '';
	$cid = secureEncode($cid);
	
	$user = $mysqli->query("SELECT id,nombre,bio FROM usuarios where id = '".$cid."'");
	
	if ($user->num_rows > 0) { 
		$us = $user->fetch_object();		
		$sm['user_crushes']['id'] = $us->id;
		$sm['user_crushes']['thumb'] = profilePhoto($us->id);
		$sm['user_crushes']['name'] = $us->nombre;
		$sm['user_crushes']['bio'] = $us->bio;		
		$crushes .= getMobilePage('crushes/crushes');
	}
	
	return $crushes;
		
}

function getTotalUsers($value) {
	global $mysqli;
	
	if($value == 1) {
		$add = "";
	}
	if($value == 2) {
		$time_now = time()-300;
		$add = "WHERE last_access >=".$time_now;
	}
	if($value == 3) {
		$add = "WHERE sexo = 0";
	}
	if($value == 4) {
		$add = "WHERE sexo = 1";
	}
	
	$query = $mysqli->query("SELECT count(*) as total FROM usuarios $add");
	$total = $query->fetch_assoc();
	
	return $total['total'];
}

function getTotalPhotos($value) {
	global $mysqli;
	
	if($value == 1) {
		$add = "";
	}
	if($value == 2) {
		$add = "WHERE aprovada = 0";
	}
	
	$query = $mysqli->query("SELECT count(*) as total FROM usuarios_fotos $add");
	$total = $query->fetch_assoc();
	
	return $total['total'];
}

function getUserInfo($uid,$value=0) {
	global $mysqli,$sm;
	
	$uid = secureEncode($uid);
	$user = $mysqli->query("SELECT * FROM usuarios WHERE id = '".$uid."'");
	if($user->num_rows == 0){
		header('Location: ' . smoothLink('index.php?page=game'));
		exit;
	}
	$u = $user->fetch_object();
	
	if (!empty($u->lang)) {
		$_SESSION['lang'] = $u->lang;
	}
	
	$u_fotos = $mysqli->query("SELECT * FROM usuarios_fotos where u_id = '".$u->id."' and aprovada = 1");
	//SEXYMETRO FUNCTION
	$user_sexymetro = sexymetro($u->id);
	//CURRENT USER
	$current_user['id'] = $u->id;
	$current_user['name'] = $u->nombre;
	$current_user['age'] = $u->edad;	
	$current_user['bio'] = $u->bio;	
	$current_user['looking'] = $u->looking;
	$current_user['gender'] = $u->sexo;	
	$current_user['email'] = $u->email;	
	$current_user['loc'] = $u->loc;	
	$current_user['lat'] = $u->lat;
	$current_user['lng'] = $u->lng;
	$current_user['password'] = $u->pass;	
	$current_user['admin'] = $u->admin;
	$current_user['pattern'] = $u->pattern;
	$current_user['sexy'] = $u->sexy;	
	$current_user['total'] = $u->total;	
	$current_user['last_access'] = $u->last_access;		
	$current_user['photo_small'] = profilePhoto($u->id);	
	$current_user['total_photos'] = $u_fotos->num_rows;		
	$current_user['sexymetro'] = $user_sexymetro['show'];
	$current_user['percent'] = $user_sexymetro['percent'];
	if($value == 1){
		$sm['profile'] = $current_user; 
		getUserProfileSocials($u->id);	
	} else if($value == 2){
		$sm['mUser'] = $current_user; 
		getUserProfileSocials($u->id);	
	}else if($value == 3){
		$sm['mail'] = $current_user;	
	}else if($value == 4){
		$sm['sexy'] = $current_user;	
	}	else{
		$sm['user'] = $current_user;	
	}
}

function getUserSocials($uid) {
	global $mysqli,$sm;
	
	$u_socials = $mysqli->query("SELECT * FROM usuarios_socials where u_id = '".$uid."'");
	if($u_socials) {
		$u_social = $u_socials->fetch_object();
		$sm['user']['facebook'] = $u_social->facebook;
		$sm['user']['twitter'] = $u_social->twitter;
		$sm['user']['skype'] = $u_social->skype;
		$sm['user']['whatsapp'] = $u_social->whatsapp;		
	}
	return $sm;			
}

function getUserProfileSocials($uid) {
	global $mysqli,$sm;
	$uid = secureEncode($uid);
	
	$u_socials = $mysqli->query("SELECT * FROM usuarios_socials where u_id = '".$uid."'");
	if($u_socials) {
		$u_social = $u_socials->fetch_object();
		$sm['profile']['facebook'] = $u_social->facebook;
		$sm['profile']['twitter'] = $u_social->twitter;
		$sm['profile']['skype'] = $u_social->skype;
		$sm['profile']['whatsapp'] = $u_social->whatsapp;		
	}
	return $sm;			
}

function checkuser($fuid,$ffname,$femail)
{
	global $mysqli,$sm;
    $check = $mysqli->query("select * from usuarios where id_facebook='$fuid'");
	$photo = "http://graph.facebook.com/".$fuid."/picture?type=large";
	$thumb = "http://graph.facebook.com/".$fuid."/picture?width=200&height=200";	
	
	if ($check->num_rows == 1){
    	$new_user = $mysqli->query("select * from usuarios where id_facebook='$fuid'");	
		$new_u = $new_user->fetch_object();		
		$_SESSION['user'] = $new_u->id;	
	} else {	
		$mysqli->query("set names 'utf8'");
		$query = "INSERT INTO usuarios (id_facebook,nombre,email,ready) VALUES 
		('$fuid','$ffname','$femail',2)";
		$mysqli->query($query);
		
    	$new_user = $mysqli->query("select * from usuarios where id_facebook='$fuid'");	
		$new_u = $new_user->fetch_object();
		$query2 = "INSERT INTO usuarios_fotos (u_id,foto,perfil,thumb,aprovada) VALUES ('".$new_u->id."','".$photo."',1,'".$thumb."',1)";
		$mysqli->query($query2);
		$_SESSION['user'] = $new_u->id;	
		$mysqli->query("INSERT INTO usuarios_socials (u_id) VALUES ('".$new_u->id."')");		
	}
}

function getUserPhoto($pid) {
	global $mysqli,$sm;
	$photos = '';
	
	$photo = $mysqli->query("SELECT * FROM usuarios_fotos where id = '".$pid."'");
	
	if ($photo->num_rows > 0) { 
		$photo_data = $photo->fetch_object();
		$sm['user_photos']['id'] = $photo_data->id;
		$sm['user_photos']['thumb'] = $photo_data->thumb;
		$sm['user_photos']['photo'] = $photo_data->foto;
		$sm['user_photos']['status'] = $photo_data->aprovada;		
		$photos .= getPage('user/user_photos');
	}
	
	return $photos;		
}

function getUserPhotoMobile($pid) {
	global $mysqli,$sm;
	$photos = '';
	
	$photo = $mysqli->query("SELECT * FROM usuarios_fotos where id = '".$pid."'");
	
	if ($photo->num_rows > 0) { 
		$photo_data = $photo->fetch_object();
		$sm['user_photos']['id'] = $photo_data->id;
		$sm['user_photos']['thumb'] = $photo_data->thumb;
		$sm['user_photos']['photo'] = $photo_data->foto;
		$sm['user_photos']['status'] = $photo_data->aprovada;		
		$photos .= getMobilePage('user/user_photos');
	}
	
	return $photos;		
}

function getAdminPhoto($pid) {
	
	global $mysqli,$sm;
	$photos = '';
	
	$photo = $mysqli->query("SELECT * FROM usuarios_fotos where id = '".$pid."'");
	
	if ($photo->num_rows > 0) { 
		$photo_data = $photo->fetch_object();
		$sm['admin_photos']['id'] = $photo_data->id;
		$sm['admin_photos']['thumb'] = $photo_data->thumb;
		$sm['admin_photos']['photo'] = $photo_data->foto;
		$sm['admin_photos']['status'] = $photo_data->aprovada;		
		$photos .= getPage('admin/admin_photos');
	}
	
	return $photos;
		
}

function alreadyVoted($u1,$u2){
	global $mysqli,$sm;
	$u1 = secureEncode($u1);
	$u2 = secureEncode($u2);
	
	$return = 0;
	$check = $mysqli->query("SELECT * FROM sexy where u1 = '".$u1."' AND u2 = '".$u2."'");
	if($check->num_rows == 1){
		$return = 1;
	}
	if($u1 == $u2){
		$return = 2;
	}
	return $return;
}

function sexyMailNotification($sexy_id)
{
	global $mysqli,$sm,$lang;
	
	getUserInfo($sexy_id,4);
	$message = file_get_contents($sm['config']['site_url'].'/themes/' . $sm['config']['theme_email'] . '/layout/crush.phtml');
	$message = str_replace('$sexy_name', $sm['sexy']['name'], $message);
	$message = str_replace('$sexy_email', $sm['sexy']['email'], $message);
	$message = str_replace('$user_name', $sm['user']['name'], $message);
	$message = str_replace('$user_age', $sm['user']['age'], $message);
	$message = str_replace('$user_photo', $sm['user']['photo_small'], $message);	
	$message = str_replace('$user_url', $sm['config']['site_url']."/index.php?page=profile&id=".$sm['user']['id'], $message);	
	$message = str_replace('$site_url', $sm['config']['site_url'], $message);	
	$message = str_replace('$site_name', $sm['config']['site_name'], $message);
	$message = str_replace('$site_email', $sm['config']['theme_url_email'], $message);	
	
	$mail = new PHPMailer;
	$mail->Host = $sm['config']['email_host'];  
	$mail->SMTPAuth = true;                              
	$mail->Username = $sm['config']['email_user'];                 
	$mail->Password = $sm['config']['email_password'];                         	
	$mail->setFrom($sm['config']['site_mail'], $sm['config']['site_name']);
	$mail->addAddress($sm['sexy']['email'],$sm['sexy']['name']);
	$mail->Subject = $sm['user']['name']." likes you!";
	$mail->msgHTML($message);
	if($mail->send()) {
	}
}

function chatMailNotification($user_id,$email_message)
{
	global $mysqli,$sm,$lang;
	
	secureEncode($email_message);
	getUserInfo($user_id,3);
	$message = file_get_contents($sm['config']['site_url'].'/themes/' . $sm['config']['theme_email'] . '/layout/message.phtml');
	$message = str_replace('$user_name', $sm['mail']['name'], $message);
	$message = str_replace('$user_email', $sm['mail']['email'], $message);
	$message = str_replace('$user_id', $sm['mail']['id'], $message);	
	$message = str_replace('$email_name', $sm['user']['name'], $message);
	$message = str_replace('$email_age', $sm['user']['age'], $message);
	$message = str_replace('$email_loc', $sm['user']['loc'], $message);	
	$message = str_replace('$email_id', $sm['user']['id'], $message);		
	$message = str_replace('$email_photo', $sm['user']['photo_small'], $message);	
	$message = str_replace('$email_message', $email_message, $message);		
	$message = str_replace('$email_url', $sm['config']['site_url']."/index.php?page=profile&id=".$sm['user']['id'], $message);	
	$message = str_replace('$site_url', $sm['config']['site_url'], $message);	
	$message = str_replace('$site_name', $sm['config']['site_name'], $message);
	$message = str_replace('$site_email', $sm['config']['theme_url_email'], $message);	
	
	$mail = new PHPMailer;
	$mail->Host = $sm['config']['email_host'];  
	$mail->SMTPAuth = true;                              
	$mail->Username = $sm['config']['email_user'];                 
	$mail->Password = $sm['config']['email_password'];                         	
	$mail->setFrom($sm['config']['site_mail'], $sm['config']['site_name']);
	$mail->addAddress($sm['mail']['email'],$sm['mail']['name']);
	$mail->Subject = "New message from ".$sm['user']['name'];
	$mail->msgHTML($message);
	if($mail->send()) {
	exit;
	}
}

function passwordReset($u_id)
{
	global $mysqli,$sm,$lang;
	
	getUserInfo($u_id,3);
	$message = file_get_contents($sm['config']['site_url'].'/themes/' . $sm['config']['theme_email'] . '/layout/forgot.phtml');
	$message = str_replace('$user_name', $sm['mail']['name'], $message);
	$message = str_replace('$user_email', $sm['mail']['email'], $message);
	$message = str_replace('$user_password', $sm['mail']['password'], $message);
	$message = str_replace('$user_url', $sm['config']['site_url']."/index.php?page=changePassword&email=".$sm['mail']['id']."&code=".md5($sm['mail']['name']), $message);
	$message = str_replace('$site_url', $sm['config']['site_url'], $message);	
	$message = str_replace('$site_name', $sm['config']['site_name'], $message);
	$message = str_replace('$site_email', $sm['config']['theme_url_email'], $message);	
	
	$mail = new PHPMailer;
	$mail->Host = $sm['config']['email_host'];  
	$mail->SMTPAuth = true;                              
	$mail->Username = $sm['config']['email_user'];                 
	$mail->Password = $sm['config']['email_password'];                         	
	$mail->setFrom($sm['config']['site_mail'], $sm['config']['site_name']);
	$mail->addAddress($sm['mail']['email'],$sm['mail']['name']);
	$mail->Subject = "Choose a new password!";
	$mail->msgHTML($message);
	if($mail->send()) {
	}
}

function newUserMail($user_name,$user_email,$user_password)
{
	global $mysqli,$sm,$lang;
	
	$message = file_get_contents($sm['config']['site_url'].'/themes/' . $sm['config']['theme_email'] . '/layout/welcome.phtml');
	$message = str_replace('$user_name', $user_name, $message);
	$message = str_replace('$user_email', $user_email, $message);
	$message = str_replace('$user_password', $user_password, $message);	
	$message = str_replace('$site_url', $sm['config']['site_url'], $message);	
	$message = str_replace('$site_name', $sm['config']['site_name'], $message);
	$message = str_replace('$site_email', $sm['config']['theme_url_email'], $message);
	
	$mail = new PHPMailer;
	$mail->Host = $sm['config']['email_host'];  
	$mail->SMTPAuth = true;                              
	$mail->Username = $sm['config']['email_user'];                 
	$mail->Password = $sm['config']['email_password'];                         	
	$mail->setFrom($sm['config']['site_mail'], $sm['config']['site_name']);
	$mail->addAddress($user_email,$user_name);
	$mail->Subject = "Welcome to ".$sm['config']['site_name'];
	$mail->msgHTML($message);
	if($mail->send()) {
	}
}

function crushes($uid){
	global $mysqli,$config;
	$result = "";
	$mysqli->query("UPDATE usuarios SET likes = 0 where id = '".$uid."'");	
	$likes = $mysqli->query("SELECT u1 FROM sexy where u2 = '".$uid."' and sexy = 1");	
	if ($likes->num_rows > 0) {
		while($li = $likes->fetch_object()){
			$user = $mysqli->query("SELECT * FROM usuarios where id = '".$li->u1."'");
			$us = $user->fetch_object();
			$loc = substr($us->loc, 0, 24);
			if(strlen($us->loc) > 24){$loc = $loc.'..';}			
			$result.= '
			 <div class="col-md-3  col-sm-4" style="padding-top:10px;">
					   <div class="quickView member noThumbs" style="background:url('.$config['theme_url'].'/pattern/'.$us->pattern.'.jpg) repeat;">
						   <div class="padding">
							   <div class="gallery">
									<a class="preview" href="#'.$us->name.'"
									onclick="info_user('.$uid.','.$us->id.','."'".$us->nombre."'".','."'".$us->bio."'".','.profilePhoto($us->id)."'".')"
									name="'.$us->id.'">								   
									<img data-quick-view="previewImage" src="'.profilePhoto($us->id).'"/></a>
							   </div>
							   <div class="info">
									<div class="name"><a href="#'.$us->name.'"
									onclick="info_user('.$uid.','.$us->id.','."'".$us->nombre."'".','."'".$us->bio."'".','."'".profilePhoto($us->id)."'".')"
									name="'.$us->id.'" style="color:#fff;">'.$us->nombre.' , <span class="age female">'.$us->edad.'</span></a> <span>'.status($us->id,11).'</span></div>
									<div class="place"><a href="#">'.$loc.'</a></div>
							   </div>					   
						   </div>
					   </div>
			  </div>
			';
		}
	}
	echo $result;
}
/* Check Functions */
function isLogged() {
    global $mysqli;
    
    if (!empty($_SESSION['user']) && is_numeric($_SESSION['user']) && $_SESSION['user'] > 0) {
        $user_id = secureEncode($_SESSION['user']);
        $query = "SELECT COUNT(id) AS count FROM usuarios WHERE id=$user_id";
        $sql_query = $mysqli->query($query);
        $sql_fetch = mysqli_fetch_assoc($sql_query);
        
        return $sql_fetch['count'];
    }
}

function getPage($page_url='') {
    global $sm, $lang;
    
    $page = './themes/' . $sm['config']['theme'] . '/layout/' . $page_url . '.phtml';
    $page_content = '';
    
    ob_start();
    include($page);
    $page_content = ob_get_contents();
    ob_end_clean();
    
    return $page_content;
}

function getMailPage($page_url='') {
    global $sm, $lang;
    
    $page = './themes/' . $sm['config']['theme_email'] . '/layout/' . $page_url . '.phtml';
    $page_content = '';
    
    ob_start();
    include($page);
    $page_content = ob_get_contents();
    ob_end_clean();
    
    return $page_content;
}

function getMobilePage($page_url='') {
    global $sm, $lang;
    
    $page = './themes/' . $sm['config']['theme_mobile'] . '/layout/' . $page_url . '.phtml';
    $page_content = '';
    
    ob_start();
    include($page);
    $page_content = ob_get_contents();
    ob_end_clean();
    
    return $page_content;
}

function getAdminPage($page_url='') {
    global $sm, $lang;
    
    $page = './themes/' . $sm['config']['theme'] . '/layout/' . $page_url . '.phtml';
    $page_content = '';
    
    ob_start();
    include($page);
    $page_content = ob_get_contents();
    ob_end_clean();
    
    return $page_content;
}

function getRandomUser(){
	global $mysqli;
	/* Get random user to show */
	$sexy_rand = $mysqli->query("SELECT * FROM usuarios where ready = 2 ORDER BY RAND() LIMIT 1");
	$sexy = $sexy_rand->fetch_object();
	
	$sexymetro = sexymetro($sexy->id);
	
	$bio = substr($sexy->bio, 0, 36);
	$bio = $bio.'...';
	$random = array();
	$random['id'] = $sexy->id;
	$random['name'] = $sexy->nombre;	
	$random['age'] = $sexy->edad;
	$random['bio'] = $bio;
	$random['sexy'] = $sexy->sexy;	
	$random['total'] = $sexy->total;	
	$random['sexymetro'] = $sexymetro['show'];
	$random['percent'] = $sexymetro['percent'];
	return $random;
}

function userGalleria($uid){
	/* Get user photos for galleria */	
	global $mysqli;	
	
	$u_foto = $mysqli->query("SELECT * FROM usuarios_fotos where u_id = '".$uid."' and aprovada = 1 order by id DESC");
	
	if ($u_foto->num_rows > 0) { 
		while($u_fotos = $u_foto->fetch_object()) {
			echo' <a href="'.$u_fotos->foto.'"><img src="'.$u_fotos->foto.'"></a>';
		}
	}	
}

function loginRandPhotos(){
	/* Show random users photos */	
	global $mysqli;	
	
	$rand_photos = $mysqli->query("SELECT thumb,u_id FROM usuarios_fotos WHERE aprovada = 1 order by RAND() LIMIT 15");
	
	if ($rand_photos->num_rows > 0) { 
		while($photo = $rand_photos->fetch_object()){
			echo'<div class="item" style="background: url ("'.$photo->thumb.'") no-repeat center top; height:180px;">
			<a href="'.smoothLink('index.php?page=profile&id='.$photo->u_id.'').'"><img class="lazyOwl" data-src="'.$photo->thumb.'"></div>';
		}	
	}
}

/* Other functions */
function smoothLink($query='') {
    global $config;
	
    $query = $config['site_url'] . '/' . $query;
	
    return $query;
}

function userChats($uid=0){
	/* get user chats */	
	global $mysqli;		

	$all_chats  = array();

	$chat2 = $mysqli->query("SELECT * FROM chat where r_id = '".$uid."' and visto = 1 order by id desc");
	$chat3 = $mysqli->query("SELECT * FROM chat where s_id = '".$uid."' order by id desc");

	if ($chat2->num_rows > 0) { 
		while($chat22= $chat2->fetch_object()){
			if(!in_array($chat22->s_id, $all_chats)){
				$all_chats[] = $chat22->s_id;						
			}
		}
	}	
	if ($chat3->num_rows > 0) { 
		while($chat33= $chat3->fetch_object()){
			if(!in_array($chat33->r_id, $all_chats)){
				$all_chats[] = $chat33->r_id;						
			}
		}
	}
	return $all_chats;
}

function secureEncode($string) {
    global $mysqli;
    $string = trim($string);
    $string = mysqli_real_escape_string($mysqli, $string);
    $string = htmlspecialchars($string, ENT_QUOTES);
    $string = str_replace('\\r\\n', '<br>',$string);
    $string = str_replace('\\r', '<br>',$string);
    $string = str_replace('\\n\\n', '<br>',$string);
    $string = str_replace('\\n', '<br>',$string);
    $string = str_replace('\\n', '<br>',$string);
    $string = stripslashes($string);
    $string = str_replace('&amp;#', '&#',$string);
    
    return $string;
}