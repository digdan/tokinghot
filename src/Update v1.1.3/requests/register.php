<?php 
require_once('../assets/includes/core.php');

if($_POST) {
	//GET INFO FROM POST
	$name = $_POST['nickname'];
	$email = $_POST['email'];	
	$password = $_POST['pass'];
	$city = $_POST['formatted_address'];
	$lat = $_POST['lat'];
	$lng = $_POST['lng'];	
	$age = $_POST['age'];	
	$gender = $_POST['gender'];
	$bio = $_POST['bio'];
	$looking = $_POST['looking'];	
	
	//Secure info
	$name = secureEncode($name);	
	$email = secureEncode($email);		
	$password = secureEncode($password);	
	$city = secureEncode($city);	
	$lat = secureEncode($lat);	
	$lng = secureEncode($lng);		
	$age = secureEncode($age);		
	$gender = secureEncode($gender);	
	$bio = secureEncode($bio);	
	$looking = secureEncode($looking);			
	
	//CHECK IF CITY IS OK
	if($city == "" || $city == NULL ){
		echo 'Error - '.$lang['register_1'];	
		exit;
	}
	//CHECK IF USER EXIST
	$email_check = $mysqli->query("SELECT * FROM usuarios WHERE email = '".$email."'");	
	if($email_check->num_rows == 1 ){
		echo 'Error - '.$lang['register_2'];	
		exit;
	} else {
		//ADD USER IN DB	
		$mysqli->query("set names 'utf8'");
		$mysqli->query("INSERT INTO usuarios (nombre,email,pass,loc,edad,sexo,bio,looking,lat,lng,ready,lang) VALUES ('".$name."', '".$email."', '".crypt($password)."', '".$city."', '".$age."', '".$gender."', '".$bio."', '".$looking."', '".$lat."', '".$lng."',1,'".$_SESSION['lang']."')");	
		//ADD USER SESSION	
		$session = $mysqli->query("SELECT id FROM usuarios WHERE email = '".$email."'");
		if($session){
			$u_s = $session->fetch_object();
			$_SESSION['user'] = $u_s->id;
			//SEND WELCOME EMAIL
			newUserMail($name,$email,$password);
			//ADD USER ID TO SCIALS TABLE	
			$mysqli->query("INSERT INTO usuarios_socials (u_id) VALUES ('".$u_s->id."')");			
		}							 
	}
}

//CLOSE DB CONNECTION
$mysqli->close();