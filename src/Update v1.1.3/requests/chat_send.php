<?php
require_once('../assets/includes/core.php');

//Get info
$message = $_POST['mensaje'];
$u1 = $_POST['uid1'];
$u2 = $_POST['uid2'];


//Secure info
$message = secureEncode($message);
$u1 = secureEncode($u1);
$u2 = secureEncode($u2);

//TIME OF THE MSG
$fecha = microtime_float();

$mysqli->query("set names 'utf8'");
$mysqli->query("INSERT INTO chat (s_id, r_id , mensaje,fecha) VALUES ('".$u1."', '".$u2."', '".$message."','".$fecha."')");	

//UPDATE CHAT MENU NOTIFICATION
$mysqli->query("UPDATE usuarios SET chat = chat+1 where id = '".$u2."'");	

//CHECK IF IS FIRST MESSAGE OF CONVERSATION FOR PREVENT SPAM
$total_m = $mysqli->query("SELECT id FROM chat WHERE s_id = '".$u1."' AND r_id = '".$u2."'");

//GET ALL CHATS
$chatt = $mysqli->query("SELECT id FROM chat where s_id = '".$u1."' and r_id = '".$u2."' OR r_id = '".$u1."' and s_id = '".$u2."'");

if($total_m->num_rows == 1 && $chatt->num_rows == 1) {
	echo '<li class="info" id="prevent_spam">
	<p>'.$lang['chat_first_message_info'].'</p>
	</li>';
}	

$user = $mysqli->query("SELECT last_access FROM usuarios where id = '".$u2."'");
$us = $user->fetch_object();
$time_now = time()-300;

//SEND CHAT MAIL NOTIFICATION TO OFFLINE USERS
if($sm['config']['chat_mail_notification'] == 1 && $us->last_access < $time_now){
	chatMailNotification($u2,$message);	
}
//CLOSE DB CONNECTION
$mysqli->close();
