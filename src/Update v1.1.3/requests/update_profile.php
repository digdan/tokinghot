<?php 
require_once('../assets/includes/core.php');

if($_POST){	

	//GET INFO FROM POST
	$name = $_POST['nickname'];
	$email = $_POST['email'];	
	$city = $_POST['formatted_address'];
	$lat = $_POST['lat'];
	$lng = $_POST['lng'];	
	$age = $_POST['age'];	
	$gender = $_POST['gender'];
	$bio = $_POST['bio'];
	$looking = $_POST['looking'];	
	$id = $_POST['uid'];
	
	//PREVENT INJECTION
	$name = secureEncode($name);	
	$email = secureEncode($email);		
	$city = secureEncode($city);	
	$lat = secureEncode($lat);	
	$lng = secureEncode($lng);		
	$age = secureEncode($age);		
	$gender = secureEncode($gender);	
	$bio = secureEncode($bio);	
	$looking = secureEncode($looking);	
	$id = secureEncode($id);	

	$mysqli->query("set names 'utf8'");
	$mysqli->query("UPDATE usuarios set nombre = '".$name."', email = '".$email."',loc = '".$city."', edad = '".$age."',sexo = '".$gender."',
				   bio = '".$bio."',looking = '".$looking."',lat = '".$lat."',lng = '".$lng."' where id = '".$id."'");
}

//CLOSE DB CONNECTION
$mysqli->close();

